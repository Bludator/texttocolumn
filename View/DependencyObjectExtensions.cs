using System.Windows;
using System.Windows.Media;

namespace TextToColumn.View
{
	static class DependencyObjectExtensions
	{
		/// <summary>
		///   Finds parent of type `T` in `VisualTree`
		/// </summary>
		/// <typeparam name="T"> type of the parent </typeparam>
		/// <param name="child"> element on which the search starts </param>
		/// <returns> </returns>
		public static T FindVisualParent<T>(this DependencyObject child) where T : DependencyObject
		{
			//get parent item
			DependencyObject parentObject = VisualTreeHelper.GetParent(child);

			//we've reached the end of the tree
			if (parentObject == null) return null;

			//check if the parent matches the type we're looking for
			if (parentObject is T parent)
				return parent;
			else
				return FindVisualParent<T>(parentObject);
		}
	}
}