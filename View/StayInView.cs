using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace TextToColumn.View
{
	/// <summary>
	///   Repositions element to stay in view
	/// </summary>
	/// <remarks> uses margin to positioning </remarks>
	class StayInView : ScrollBasedBehaviour<StayInView>
	{
		#region Attached properties

		/// <summary>
		///   getter for <see cref="VerticallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static bool GetVertically(DependencyObject obj) => (bool)obj.GetValue(VerticallyProperty);

		/// <summary>
		///   setter for <see cref="VerticallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static void SetVertically(DependencyObject obj, bool value) => obj.SetValue(VerticallyProperty, value);

		/// <summary>
		///   if set to `true`
		/// </summary>
		public static readonly DependencyProperty VerticallyProperty =
			 DependencyProperty.RegisterAttached("Vertically", typeof(bool), typeof(StayInView), new PropertyMetadata(false, VerticalOnOffPropertyChangedHandler));

		/// <summary>
		///   getter for <see cref="HorizontallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static bool GetHorizontally(DependencyObject obj) => (bool)obj.GetValue(HorizontallyProperty);

		/// <summary>
		///   setter for <see cref="HorizontallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static void SetHorizontally(DependencyObject obj, bool value) => obj.SetValue(HorizontallyProperty, value);

		/// <summary>
		/// </summary>
		public static readonly DependencyProperty HorizontallyProperty =
			 DependencyProperty.RegisterAttached("Horizontally", typeof(bool), typeof(StayInView), new PropertyMetadata(false, HorizontalOnOffPropertyChangedHandler));

		/// <summary>
		///   getter for <see cref="VerticalAlignmentProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static VerticalAlignment GetVerticalAlignment(DependencyObject obj) => (VerticalAlignment)obj.GetValue(VerticalAlignmentProperty);

		/// <summary>
		///   setter for <see cref="VerticalAlignmentProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static void SetVerticalAlignment(DependencyObject obj, VerticalAlignment value) => obj.SetValue(VerticalAlignmentProperty, value);

		/// <summary>
		/// </summary>
		public static readonly DependencyProperty VerticalAlignmentProperty =
			 DependencyProperty.RegisterAttached("VerticalAlignment", typeof(VerticalAlignment), typeof(StayInView), new PropertyMetadata(VerticalAlignment.Center,
				  (o, e) => { if (o is FrameworkElement element && registered.TryGetValue(element, out var stayInView)) stayInView.Update(true, false); }));

		/// <summary>
		///   getter for <see cref="HorizontalAlignmentProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static HorizontalAlignment GetHorizontalAlignment(DependencyObject obj) => (HorizontalAlignment)obj.GetValue(HorizontalAlignmentProperty);

		/// <summary>
		///   setter for <see cref="HorizontalAlignmentProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static void SetHorizontalAlignment(DependencyObject obj, HorizontalAlignment value) => obj.SetValue(HorizontalAlignmentProperty, value);

		/// <summary>
		/// </summary>
		public static readonly DependencyProperty HorizontalAlignmentProperty =
			 DependencyProperty.RegisterAttached("HorizontalAlignment", typeof(HorizontalAlignment), typeof(StayInView), new PropertyMetadata(HorizontalAlignment.Center,
				  (o, e) => { if (o is FrameworkElement element && registered.TryGetValue(element, out var stayInView)) stayInView.Update(false, true); }));

		#endregion Attached properties

		///<inheritdoc/>
		public override bool IsVertical => ElementWeakRef.TryGetTarget(out var element) && GetVertically(element);

		/// <summary>
		///   value of <see cref="VerticalAlignmentProperty"/> on <see cref="ElementWeakRef"/>
		/// </summary>
		public VerticalAlignment VerticalAlignment => ElementWeakRef.TryGetTarget(out var element) ? GetVerticalAlignment(element) : 0;

		///<inheritdoc/>
		public override bool IsHorizontal => ElementWeakRef.TryGetTarget(out var element) && GetHorizontally(element);

		/// <summary>
		///   value of <see cref="HorizontalAlignmentProperty"/> on <see cref="ElementWeakRef"/>
		/// </summary>
		public HorizontalAlignment HorizontalAlignment => ElementWeakRef.TryGetTarget(out var element) ? GetHorizontalAlignment(element) : 0;

		/// <summary>
		///   updates position of element so it stays in view
		/// </summary>
		/// <param name="verticalChange"> update vertical position </param>
		/// <param name="HorizontalChnage"> update horizontal position </param>
		public override void Update(bool verticalChange, bool HorizontalChnage)
		{
			if (ElementWeakRef.TryGetTarget(out var element) && ScrollViewerWeakRef.TryGetTarget(out var scrollViewer))
			{
				Thickness margin = element.Margin;
				if (IsVertical && verticalChange)
				{
					margin = VerticalReposition(element, scrollViewer, margin);
				}
				if (IsHorizontal && HorizontalChnage)
				{
					margin = HorizontalRepositon(element, scrollViewer, margin);
				}

				var move = new ThicknessAnimation()
				{
					To = margin,
					Duration = TimeSpan.FromMilliseconds(800),
					EasingFunction = new ElasticEase()
					{
						EasingMode = EasingMode.EaseOut,
						Springiness = 8,
						Oscillations = 2
					}
				};

				element.BeginAnimation(ContentControl.MarginProperty, move);
			}
		}

		private Thickness HorizontalRepositon(FrameworkElement element, ScrollViewer scrollViewer, Thickness margin)
		{
			switch (HorizontalAlignment)
			{
				case HorizontalAlignment.Left:
					margin.Left = scrollViewer.HorizontalOffset;
					break;

				case HorizontalAlignment.Center:
					margin.Left = scrollViewer.HorizontalOffset + (scrollViewer.ViewportWidth - element.ActualWidth) / 2;
					break;

				case HorizontalAlignment.Right:
					margin.Left = scrollViewer.HorizontalOffset + scrollViewer.ViewportWidth - element.ActualWidth;
					break;

				case HorizontalAlignment.Stretch:
					margin.Left = scrollViewer.ExtentWidth;
					margin.Right = scrollViewer.ExtentWidth - (scrollViewer.HorizontalOffset + scrollViewer.ViewportWidth);
					break;

				default:
					break;
			}

			return margin;
		}

		private Thickness VerticalReposition(FrameworkElement element, ScrollViewer scrollViewer, Thickness margin)
		{
			switch (VerticalAlignment)
			{
				case VerticalAlignment.Top:
					margin.Top = scrollViewer.VerticalOffset;
					break;

				case VerticalAlignment.Center:
					margin.Top = scrollViewer.VerticalOffset + (scrollViewer.ViewportHeight - element.ActualHeight) / 2;
					break;

				case VerticalAlignment.Bottom:
					margin.Top = scrollViewer.VerticalOffset + scrollViewer.ViewportHeight - element.ActualHeight;
					break;

				case VerticalAlignment.Stretch:
					margin.Top = scrollViewer.ExtentHeight;
					margin.Bottom = scrollViewer.ExtentHeight - (scrollViewer.VerticalOffset + scrollViewer.ViewportHeight);
					break;

				default:
					break;
			}

			return margin;
		}
	}
}