using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace TextToColumn.View
{
	/// <summary>
	///   Base class for attached behaviors reacting to the scrolling and resizing of scroll element
	/// </summary>
	/// <typeparam name="T"> type of derived class </typeparam>
	abstract class ScrollBasedBehaviour<T> where T : ScrollBasedBehaviour<T>, new()
	{
		#region Static members
		/// <summary>
		///   Maps <see cref="T"/> instances to the `FrameworkElement` with <see cref="T"/>'s attached properties
		/// </summary>
		protected static ConditionalWeakTable<FrameworkElement, T> registered = new ConditionalWeakTable<FrameworkElement, T>();

		/// <summary>
		///   Creates/finds instance of <see cref="T"/> and set it up for vertical use. Used along with
		///   <see cref="HorizontalOnOffPropertyChangedHandler"/> to manage <see cref="T"/> instance lifetime
		/// </summary>
		/// <remarks>
		///   Typically passed in `PropertyMetadata` as `PropertyChangedCallback` in definition of attached properties.
		/// </remarks>
		/// <param name="obj"> </param>
		/// <param name="args"> </param>
		protected static void VerticalOnOffPropertyChangedHandler(DependencyObject obj, DependencyPropertyChangedEventArgs args)
						=> OnOffPropertyChangedHandler(obj, args, true);

		/// <summary>
		///   Creates/finds instance of <see cref="T"/> and set it up for horizontal use. Used along with
		///   <see cref="VerticalOnOffPropertyChangedHandler"/> to manage <see cref="T"/> instance lifetime
		/// </summary>
		/// <remarks>
		///   Typically passed in `PropertyMetadata` as `PropertyChangedCallback` in definition of attached properties.
		/// </remarks>
		/// <param name="obj"> </param>
		/// <param name="args"> </param>
		protected static void HorizontalOnOffPropertyChangedHandler(DependencyObject obj, DependencyPropertyChangedEventArgs args)
			 => OnOffPropertyChangedHandler(obj, args, false);

		private static void OnOffPropertyChangedHandler(DependencyObject obj, DependencyPropertyChangedEventArgs args, bool isVertical)
		{
			if (obj is FrameworkElement element)
			{
				if (element.IsLoaded)
				{
					OnOff(element, isVertical);
				}
				else
				{
					element.Loaded += (o, e) => OnOff(element, isVertical);
				}
			}
			else
			{
				throw new Exception(typeof(T).ToString() + " attached properties must be on FrameworkElement");
			}
		}

		private static void OnOff(FrameworkElement element, bool isVertical)
		{
			ScrollViewer scrollViewer = element.FindVisualParent<ScrollViewer>();
			if (scrollViewer != null)
			{
				if (!registered.TryGetValue(element, out var stayInView))
				{
					stayInView = new T();
					stayInView.SetRefs(scrollViewer, element);
					stayInView.Register();
					registered.Add(element, stayInView);
				}
				else
				{
					if (!stayInView.IsVertical && !stayInView.IsHorizontal)
					{
						stayInView.Unregister();
					}
				}
				stayInView.Update(isVertical, !isVertical);
			}
			else
			{
				throw new Exception("StayInView attached properties must be on FrameworkElement nested inside ScrollViewer");
			}
		}

		#endregion Static members

		/// <summary>
		///   Do we react on vertical scroll
		/// </summary>
		public abstract bool IsVertical { get; }

		/// <summary>
		///   Do we react on horizontal scroll
		/// </summary>
		public abstract bool IsHorizontal { get; }

		/// <summary>
		///   Weak reference to the element
		/// </summary>
		protected WeakReference<FrameworkElement> ElementWeakRef { get; private set; }

		/// <summary>
		///   Weak reference to the `ScrollViewer`
		/// </summary>
		protected WeakReference<ScrollViewer> ScrollViewerWeakRef { get; private set; }

		public ScrollBasedBehaviour()
		{ }

		private void SetRefs(ScrollViewer scrollViewer, FrameworkElement element)
		{
			this.ScrollViewerWeakRef = new WeakReference<ScrollViewer>(scrollViewer);
			this.ElementWeakRef = new WeakReference<FrameworkElement>(element);
		}

		/// <summary>
		///   Called on registering scroll events
		/// </summary>
		protected virtual void OnRegister() { }

		private void Register()
		{
			if (ScrollViewerWeakRef.TryGetTarget(out var scrollViewer))
			{
				WeakEventManager<ScrollViewer, ScrollChangedEventArgs>.AddHandler(scrollViewer, "ScrollChanged", ScrollViewer_ScrollChanged);
				WeakEventManager<ScrollViewer, SizeChangedEventArgs>.AddHandler(scrollViewer, "SizeChanged", ScrollViewer_SizeChanged);
				OnRegister();
			}
		}

		/// <summary>
		///   Called on unregistering scroll events
		/// </summary>
		protected virtual void OnUnregister() { }

		private void Unregister()
		{
			if (ScrollViewerWeakRef.TryGetTarget(out var scrollViewer))
			{
				WeakEventManager<ScrollViewer, ScrollChangedEventArgs>.RemoveHandler(scrollViewer, "ScrollChanged", ScrollViewer_ScrollChanged);
				WeakEventManager<ScrollViewer, SizeChangedEventArgs>.RemoveHandler(scrollViewer, "SizeChanged", ScrollViewer_SizeChanged);
				OnUnregister();
			}
		}

		private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
		{
			if (ScrollViewerWeakRef.TryGetTarget(out var scrollViewer) && ReferenceEquals(scrollViewer, e.Source))
			{
				Update(
					 e.ExtentHeightChange != 0 || e.ViewportHeightChange != 0 || e.VerticalChange != 0,
					 e.ExtentWidthChange != 0 || e.ViewportWidthChange != 0 || e.HorizontalChange != 0);
			}
		}

		private void ScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (ScrollViewerWeakRef.TryGetTarget(out var scrollViewer) && ReferenceEquals(scrollViewer, e.Source))
			{
				Update(e.HeightChanged, e.WidthChanged);
			}
		}

		/// <summary>
		///   This method is called on scroll
		/// </summary>
		/// <param name="verticalChange"> Has vertical scroll changed? </param>
		/// <param name="horizontalChnage"> Has horizontal scroll changed? </param>
		abstract public void Update(bool verticalChange, bool horizontalChnage);
	}
}