using GongSolutions.Wpf.DragDrop;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TextToColumn.IO;
using TextToColumn.TableCommands;
using TextToColumn.ViewModel;

namespace TextToColumn.View
{
	/// <summary>
	///   Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, IDropTarget
	{
		private readonly TableVM vm;

		public MainWindow(TableVM vm)
		{
			this.DataContext = this.vm = vm;
			InitializeComponent();
			vm.PropertyChanged += (s, e) => VMSelectedColumnsChanged(s, e, Table);
		}

		private bool IsViewUpdatingColumns = false;
		private bool IsVMUpdatingColumns = false;

		private void Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (IsVMUpdatingColumns) return;
			IsViewUpdatingColumns = true;

			vm.SelectedColumns = ((ListBox)sender).SelectedItems.Cast<ColumnVM>().ToArray();
			IsViewUpdatingColumns = false;
		}

		private void VMSelectedColumnsChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e, ListBox listbox)
		{
			if (IsViewUpdatingColumns) return;
			if (ReferenceEquals(sender, vm) && e.PropertyName == nameof(vm.SelectedColumns))
			{
				IsVMUpdatingColumns = true;
				var ViewSelectedItems = listbox.SelectedItems;
				ViewSelectedItems.Clear();
				foreach (var item in vm.SelectedColumns)
				{
					ViewSelectedItems.Add(item);
				}
				IsVMUpdatingColumns = false;
			}
		}

		private void LoadBtn_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				List<IEnumerable<string>> sources = new List<IEnumerable<string>>();
				foreach (var fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
				{
					sources.AddRange(new Text(System.IO.File.ReadLines(fileName)).Load());
				}
				vm.Table.AddSources(sources);
			}
		}

		void IDropTarget.DragOver(IDropInfo dropInfo)
		{
			dropInfo.DropTargetAdorner = DropTargetAdorners.Insert;
			dropInfo.Effects = DragDropEffects.Move;
		}

		void IDropTarget.Drop(IDropInfo dropInfo)
		{
			vm.Move(dropInfo.InsertIndex);
		}
	}
}