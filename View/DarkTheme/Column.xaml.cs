using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace TextToColumn.View
{
	public partial class Column : ResourceDictionary
	{
		public Column()
		{
			InitializeComponent();
		}

		public static T FindAncestor<T>(DependencyObject child) where T : DependencyObject
		{
			DependencyObject parentObject = VisualTreeHelper.GetParent(child);
			if (parentObject == null) return null;
			if (parentObject is T parent) return parent;
			else return FindAncestor<T>(parentObject);
		}

		private void TextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			TextBox textbox = ((TextBox)sender);
			if (textbox.IsKeyboardFocused) return;
			if (e.ClickCount <= 1)
			{
				e.Handled = true;
				// creates bubbling event
				var relaunchedEvent = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice)
				{
					RoutedEvent = Mouse.MouseDownEvent,
					Source = e.OriginalSource,
				};
				FindAncestor<UIElement>(textbox).RaiseEvent(relaunchedEvent);
			}
			else
			{
				RoutedEventHandler handler = null;
				textbox.SelectionChanged += handler = (s, a) =>
				{
					textbox.SelectionChanged -= handler;
					textbox.SelectionLength = 0;
					textbox.CaretIndex = textbox.Text.Length;
				};
			}
		}
	}
}