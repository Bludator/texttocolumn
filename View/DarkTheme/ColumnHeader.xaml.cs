using System.Windows.Controls;

namespace TextToColumn.View.DarkTheme
{
	/// <summary>
	///   Interaction logic for ColumnHeader.xaml
	/// </summary>
	public partial class ColumnHeader : UserControl
	{
		public ColumnHeader()
		{
			InitializeComponent();
		}
	}
}