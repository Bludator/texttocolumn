using System.Collections.Generic;
using TextToColumn.ViewModel;

namespace TextToColumn.View.Sample
{
	/// <summary>
	///   Sample Table for VS Designer
	/// </summary>
	public class Table
	{
		public IList<ColumnVM> Columns { get; }

		private static ColumnVM GetCol(State state) => new ColumnVM(null, new TextToColumn.EditableColumn(state, LoremIpsum()));

		private static IEnumerable<string> LoremIpsum()
		{
			const string LoremIpsum = @"
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
			return LoremIpsum.Split(' ');
		}

		public bool ShowRemoved => true;

		public Table()
		{
			Columns = new ColumnVM[] {
					 GetCol(State.Ok),
					 GetCol(State.ExplicitlyRemoved),
					 GetCol(State.Ok),
					 GetCol(State.Placeholder),
                //new ColumnVM(new PlaceHolderColumn(null)),
                GetCol(State.Ok),
				};
		}
	}
}