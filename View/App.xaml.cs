using Microsoft.Win32;
using System.IO;
using System.Windows;
using TextToColumn.IO;
using TextToColumn.ViewModel;

namespace TextToColumn.View
{
	/// <summary>
	///   Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private void App_Startup(object sender, StartupEventArgs e)
		{
			System.Windows.Input.CommandManager.RequerySuggested += (s, e) => Command.RaiseAllCanExecuteChanged();
			if (e.Args.Length == 0)
			{
				TableVM main = new TableVM(() =>
				{
					OpenFileDialog dialog = new OpenFileDialog() { DefaultExt = ".txt" };
					if (dialog.ShowDialog() == true)
					{
						return new Text(File.ReadLines(dialog.FileName).Cache()).Load();
					}
					return null;
				}, (TableVM table) =>
				{
					SaveFileDialog dialog = new SaveFileDialog() { FileName = "Columns", DefaultExt = ".csv" };
					if (dialog.ShowDialog() == true)
					{
						new CSV(data => File.AppendAllLines(dialog.FileName, data)).Save(table);
					}
				});
				MainWindow mainWindow = new MainWindow(main);
				mainWindow.Show();
			}
			else
			{
			}
		}
	}
}