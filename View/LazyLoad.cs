using System.Windows;
using System.Windows.Input;

namespace TextToColumn.View
{
	/// <summary>
	/// </summary>
	class LazyLoad : ScrollBasedBehaviour<LazyLoad>
	{
		#region Attached properties

		/// <summary>
		///   getter for <see cref="VerticallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static ICommand GetVertically(DependencyObject obj) => (ICommand)obj.GetValue(VerticallyProperty);

		/// <summary>
		///   setter for <see cref="VerticallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static void SetVertically(DependencyObject obj, ICommand value) => obj.SetValue(VerticallyProperty, value);

		/// <summary>
		/// </summary>
		public static readonly DependencyProperty VerticallyProperty =
			 DependencyProperty.RegisterAttached("Vertically", typeof(ICommand), typeof(LazyLoad), new PropertyMetadata(null, VerticalOnOffPropertyChangedHandler));

		/// <summary>
		///   getter for <see cref="HorizontallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static ICommand GetHorizontally(DependencyObject obj) => (ICommand)obj.GetValue(HorizontallyProperty);

		/// <summary>
		///   setter for <see cref="HorizontallyProperty"/>
		/// </summary>
		/// <param name="obj"> </param>
		/// <returns> </returns>
		public static void SetHorizontally(DependencyObject obj, ICommand value) => obj.SetValue(HorizontallyProperty, value);

		/// <summary>
		/// </summary>
		public static readonly DependencyProperty HorizontallyProperty =
			 DependencyProperty.RegisterAttached("Horizontally", typeof(ICommand), typeof(LazyLoad), new PropertyMetadata(null, HorizontalOnOffPropertyChangedHandler));

		#endregion Attached properties

		/// <summary>
		///   value of <see cref="VerticallyProperty"/> on <see cref="ElementWeakRef"/>
		/// </summary>
		public ICommand VerticalLoad => ElementWeakRef.TryGetTarget(out var element) ? GetVertically(element) : null;

		/// <summary>
		///   value of <see cref="HorizontallyProperty"/> on <see cref="ElementWeakRef"/>
		/// </summary>
		public ICommand HorizontalLoad => ElementWeakRef.TryGetTarget(out var element) ? GetHorizontally(element) : null;

		/// <summary>
		///   Is <see cref="VerticalLoad"/> set
		/// </summary>
		public override bool IsVertical => VerticalLoad != null;

		/// <summary>
		///   Is <see cref="HorizontalLoad"/> set
		/// </summary>
		public override bool IsHorizontal => HorizontalLoad != null;

		private const float ViewportToPreloadRatio = 1;

		protected override void OnRegister()
		{
			if (ElementWeakRef.TryGetTarget(out var element))
			{
				WeakEventManager<FrameworkElement, SizeChangedEventArgs>.AddHandler(element, "SizeChanged", FrameworkElement_SizeChanged);
			}
		}

		protected override void OnUnregister()
		{
			if (ElementWeakRef.TryGetTarget(out var element))
			{
				WeakEventManager<FrameworkElement, SizeChangedEventArgs>.RemoveHandler(element, "SizeChanged", FrameworkElement_SizeChanged);
			}
		}

		private void FrameworkElement_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (ElementWeakRef.TryGetTarget(out var element) && ReferenceEquals(element, e.Source))
			{
				Update(e.HeightChanged, e.WidthChanged);
			}
		}

		/// <summary>
		///   Loads more data with <see cref="VerticalLoad"/> and <see cref="HorizontalLoad"/> when user reach the end of
		///   the `ScrollViewer`
		/// </summary>
		/// <param name="verticalChange"> </param>
		/// <param name="HorizontalChnage"> </param>
		public override void Update(bool verticalChange, bool HorizontalChnage)
		{
			if (ScrollViewerWeakRef.TryGetTarget(out var scrollViewer) && ElementWeakRef.TryGetTarget(out var element))
			{
				if (verticalChange && VerticalLoad != null)
				{
					if (VerticalLoad.CanExecute(this) &&
						 element.ActualHeight - scrollViewer.VerticalOffset - scrollViewer.ViewportHeight < ViewportToPreloadRatio * scrollViewer.ViewportHeight)
					{
						VerticalLoad.Execute(this);
					}
				}
				if (HorizontalChnage && HorizontalLoad != null)
				{
					if (HorizontalLoad.CanExecute(this) &&
						 element.ActualWidth - scrollViewer.HorizontalOffset - scrollViewer.ViewportWidth < ViewportToPreloadRatio * scrollViewer.ViewportWidth)
					{
						HorizontalLoad.Execute(this);
					}
				}
			}
		}
	}
}