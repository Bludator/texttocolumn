using FluentAssertions;
using System;
using TextToColumn.CellsTransforms;
using Xunit;

namespace TextToColumn.Tests.TableCommands.CellsTransforms
{
	public class RegexGroupsTest
	{
		[Theory]
		[InlineData(new[] { "test#", "test", "#test" }, @"#(.*?)#")]
		[InlineData(new[] { "", "test" }, @"(x?)")]
		[InlineData(new[] { "#test#", "test" }, @"(#.*?#)")]
		[InlineData(new[] { "test", "#test#" }, @"(#.*?#)")]
		public void Transform_RegexInText_ReturnSplitted(string[] expectedColumns, string pattern)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			RegexGroups regex = new RegexGroups(pattern, false);

			//A
			(string, State)[] results = regex.Transform(inputText);

			//A

			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Theory]
		[InlineData(new[] { "tes", "t,t", "es", "t.t", "est" }, @"(t.t)")]
		[InlineData(new[] { "overlay,o", "verlay,overlay" }, @"(overlay,o)")]
		public void Transform_RegexRepeating_ReturnSplitted(string[] expectedColumns, string pattern)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			RegexGroups regex = new RegexGroups(pattern, true);

			//A
			(string, State)[] results = regex.Transform(inputText);

			//A

			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Fact]
		public void Transform_NestedGroups_ReturnSplitted()
		{
			//A
			string[] inputText = new[] { "1 2 3 4 5 6 7" };
			RegexGroups regex = new RegexGroups("1 (2 (3 4) 5) 6", false);

			//A
			(string, State)[] results = regex.Transform(inputText);

			//A
			Assert.Equal(new[] { ("1 ", State.ImplicitlyRemoved), ("2 3 4 5", State.Ok), ("3 4", State.Ok), (" 6 7", State.ImplicitlyRemoved) }, results);
		}

		[Fact]
		public void Transform_EmptyNestedGroups_ReturnSplitted()
		{
			//A
			string[] inputText = new[] { "1 2 3" };
			RegexGroups regex = new RegexGroups("1 (2 (9?))3", false);

			//A
			(string, State)[] results = regex.Transform(inputText);

			//A
			Assert.Equal(new[] { ("1 ", State.ImplicitlyRemoved), ("2 ", State.Ok), ("", State.Ok), ("3", State.ImplicitlyRemoved) }, results);
		}

		[Theory]
		[InlineData(new[] { "test,test,test" }, @"(;.*.;.;.)")]
		[InlineData(new[] { "test,test,test" }, @"test")]
		public void Transform_RegexGroupNotInText_DoNothingReturn(string[] expectedColumns, string pattern)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			RegexGroups regex = new RegexGroups(pattern, false);

			//A
			(string, State)[] results = regex.Transform(inputText);

			//A

			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Theory]
		[InlineData(new[] { State.Ok, State.ImplicitlyRemoved }, @"(Lorem)")]
		[InlineData(new[] { State.ImplicitlyRemoved, State.Ok, State.ImplicitlyRemoved, State.Ok, State.ImplicitlyRemoved }, @"(ip)")]
		public void Transform__CorrectState(State[] expectedStates, string pattern)
		{
			//A
			const string inputText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit,";
			RegexGroups regex = new RegexGroups(pattern, true);

			//A
			(string, State)[] results = regex.Transform(new[] { inputText });

			//A
			Assert.Equal(expectedStates, Shared.SkipData(results));
		}

		[Fact]
		public void Transform_null_ReturnsNull()
		{
			//A
			RegexGroups regex = new RegexGroups("x", false);

			//A
			(string, State)[] results = regex.Transform(new string[] { null });

			//A
			results.Should().AllBeEquivalentTo<(string, State)>((null, State.ImplicitlyRemoved));
		}

		[Theory]
		[InlineData(null)]
		[InlineData("")]
		public void Transform_invalidPattern_throws(string pattern)
		{
			//A
			Action act = () => new RegexGroups(pattern, false);

			//A
			act.Should().Throw<ArgumentException>();
		}
	}
}