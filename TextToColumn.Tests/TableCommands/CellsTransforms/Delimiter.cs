using FluentAssertions;
using System;
using TextToColumn.CellsTransforms;
using Xunit;

namespace TextToColumn.Tests.TableCommands.CellsTransforms
{
	public class DelimiterTest
	{
		[Theory]
		[InlineData(new[] { "test", "...", "test...test" }, "...")]
		[InlineData(new[] { "test", ";" }, ";")]
		[InlineData(new[] { "", ";", "test" }, ";")]
		public void Transform_DelimiterInText_ReturnSplitted(string[] expectedColumns, string delimiterStr)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Delimiter delimiter = new Delimiter(delimiterStr, false);

			//A
			(string, State)[] results = delimiter.Transform(inputText);

			//A
			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Theory]
		[InlineData(new[] { "test", ",", "test", ",", "test" }, ",")]
		[InlineData(new[] { "", "ab", "", "ab", "a" }, "ab")]
		public void Transform_DelimiterRepeating_ReturnSplitted(string[] expectedColumns, string delimiterStr)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Delimiter delimiter = new Delimiter(delimiterStr, true);

			//A
			(string, State)[] results = delimiter.Transform(inputText);

			//A
			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Theory]
		[InlineData(new[] { "test, test, test" }, ";")]
		[InlineData(new[] { "" }, ";")]
		public void Transform_DelimiterNotInText_DoNothingReturn(string[] expectedColumns, string delimiterStr)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Delimiter delimiter = new Delimiter(delimiterStr, false);

			//A
			(string, State)[] results = delimiter.Transform(inputText);

			//A
			Assert.Equal(expectedColumns, Shared.SkipState(results));
			Shared.SkipData(results).Should().AllBeEquivalentTo(State.Ok);
		}

		[Theory]
		[InlineData(new[] { "test", ",", "test", ",", "test" }, ",")]
		[InlineData(new[] { "test, test, test" }, ";")]
		public void Transform_Everything_ReturnCorrectState(string[] expectedColumns, string delimiterStr)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Delimiter delimiter = new Delimiter(delimiterStr, true);

			//A
			(string, State)[] results = delimiter.Transform(inputText);

			//A
			State state = State.Ok;
			foreach (var item in results)
			{
				Assert.Equal(state, item.Item2);
				state = (State)((int)++state % 2); //switching state 0, 1
			}
		}

		[Fact]
		public void Transform_null_ReturnsNull()
		{
			//A
			Delimiter delimiter = new Delimiter("x", false);

			//A
			(string, State)[] results = delimiter.Transform(new string[] { null });

			//A
			results.Should().AllBeEquivalentTo<(string, State)>((null, State.Ok));
		}

		[Theory]
		[InlineData(null)]
		[InlineData("")]
		public void Transform_invalidDelimeter_throws(string delimiter)
		{
			//A
			Action act = () => new Delimiter(delimiter, false);

			//A
			act.Should().Throw<ArgumentException>();
		}
	}
}