using FluentAssertions;
using System;
using TextToColumn.CellsTransforms;
using Xunit;

namespace TextToColumn.Tests.TableCommands.CellsTransforms
{
	public class OffsetTest
	{
		[Theory]
		[InlineData(new[] { "12345", "6789" }, 5)]
		[InlineData(new[] { "1234567890" }, 10)]
		[InlineData(new[] { "1234567890" }, 20)]
		public void Transform_ValidOffset_ReturnSplitted(string[] expectedColumns, int length)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Offset offset = new Offset(length, false);

			//A
			(string, State)[] results = offset.Transform(inputText);

			//A
			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Theory]
		[InlineData(new[] { "1234", "5678", "90" }, 4)]
		[InlineData(new[] { "12345", "67890" }, 5)]
		public void Transform_OffsetRepeating_ReturnSplitted(string[] expectedColumns, int length)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Offset offset = new Offset(length, true);

			//A
			(string, State)[] results = offset.Transform(inputText);

			//A
			Assert.Equal(expectedColumns, Shared.SkipState(results));
		}

		[Theory]
		[InlineData("1234567890", -10)]
		[InlineData("1234567890", 0)]
		public void Transform_NotPositiveOffset_Throw(string inputText, int length)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				//A
				Offset offset = new Offset(length, false);
				//A
				offset.Transform(new[] { inputText });
			});
		}

		[Theory]
		[InlineData(new[] { "12345", "67890" }, 5)]
		[InlineData(new[] { "1234567890" }, 10)]
		[InlineData(new[] { "1234", "5678", "90" }, 4)]
		public void Transform__NotSetState(string[] expectedColumns, int length)
		{
			//A
			string[] inputText = new[] { string.Concat(expectedColumns) };
			Offset offset = new Offset(length, true);

			//A
			(string, State)[] results = offset.Transform(inputText);

			//A
			foreach (var item in results)
			{
				Assert.Equal(State.Ok, item.Item2);
			}
		}

		[Fact]
		public void Transform_null_ReturnsNull()
		{
			//A
			Offset delimiter = new Offset(10, false);

			//A
			(string, State)[] results = delimiter.Transform(new string[] { null });

			//A
			results.Should().AllBeEquivalentTo<(string, State)>((null, State.Ok));
		}

		[Theory]
		[InlineData(-1)]
		[InlineData(0)]
		public void Transform_invalidDelimeter_throws(int offset)
		{
			//A
			Action act = () => new Offset(offset, false);

			//A
			act.Should().ThrowExactly<ArgumentOutOfRangeException>();
		}
	}
}