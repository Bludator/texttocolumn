namespace TextToColumn.Tests.TableCommands.CellsTransforms
{
	internal static class Shared
	{
		internal static string[] SkipState((string, State)[] cols)
		{
			string[] result = new string[cols.Length];
			for (int i = 0; i < cols.Length; i++)
			{
				result[i] = cols[i].Item1;
			}
			return result;
		}

		internal static State[] SkipData((string, State)[] cols)
		{
			State[] result = new State[cols.Length];
			for (int i = 0; i < cols.Length; i++)
			{
				result[i] = cols[i].Item2;
			}
			return result;
		}
	}
}