using FluentAssertions;
using System;
using TextToColumn.CellsTransforms;
using Xunit;

namespace TextToColumn.Tests.TableCommands.CellsTransforms
{
	public class MergeTest
	{
		[Fact]
		public void Transform_MultipleCells_Merge()
		{
			//A
			string[] input = new[] { "1", "2", "3" };
			Merge merge = new Merge();

			//A
			(string, State)[] results = merge.Transform(input);

			//A
			results.Should().HaveCount(1);
			results[0].Should().Be(("123", State.Ok));
		}

		[Fact]
		public void Transform_OneCell_KeepAsItIs()
		{
			//A
			string[] input = new[] { "1" };
			Merge merge = new Merge();

			//A
			(string, State)[] results = merge.Transform(input);

			//A
			results.Should().HaveCount(1);
			results[0].Should().Be(("1", State.Ok));
		}

		[Fact]
		public void Transform_emptyInput_returnEmpty()
		{
			//A
			Merge merge = new Merge();

			//A
			(string, State)[] results = merge.Transform(new string[0]);

			//A
			results.Should().BeEmpty();
		}

		[Fact]
		public void Transform_nullInput_Throw()
		{
			//A
			Merge merge = new Merge();

			//A
			Action act = () => merge.Transform(null);

			//A
			act.Should().Throw<ArgumentNullException>();
		}
	}
}