using Xunit;

namespace TextToColumn.Tests.TableCommands.CellsTransforms
{
	public class Others
	{
		[Theory]
		[InlineData("This is", "my test")]
		[InlineData("", "This is my test")]
		[InlineData("This is my test", "")]
		public void SplitOn__(string first, string second)
		{
			//A
			string merged = first + second;
			//A
			string result1 = merged.SplitOn(first.Length, out string result2);
			//A
			Assert.Equal(first, result1);
			Assert.Equal(second, result2);
		}
	}
}