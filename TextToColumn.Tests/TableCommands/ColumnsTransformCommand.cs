using FakeItEasy;
using FluentAssertions;
using System;
using System.Collections.Generic;

using System.Linq;
using TextToColumn.TableCommands;
using Xunit;
using static TextToColumn.Tests.Utilities;

namespace TextToColumn.Tests.TableCommands
{
	public class ColumnsTransformCommandTest
	{
		private IList<IColumn> table;
		private IList<IColumn> originalTable;
		private IList<ColumnStruct> subColumns;
		private IColumnsTransform transform;
		private ColumnsTransformCommand transformCommand;

		private void Initialize(bool HasConstantSubColums)
		{
			table = A.CollectionOfDummy<IColumn>(4);
			originalTable = table.ToArray();
			subColumns = new List<ColumnStruct>(){
					 new ColumnStruct(State.Ok,A.Dummy<IEnumerable<string>>()),
					 new ColumnStruct(State.ImplicitlyRemoved,A.Dummy<IEnumerable<string>>()),
					 new ColumnStruct(State.Ok,A.Dummy<IEnumerable<string>>())
				};

			transform = A.Fake<IColumnsTransform>();
			A.CallTo(() => transform.HasConstantSubColums).Returns(HasConstantSubColums);
			A.CallTo(() => transform.SubColumns).Returns(subColumns.ToArray());
			A.CallTo(() => transform.Sources).Returns(new[] { table[1], table[2] });
			A.CallTo(() => transform.Transform(A<IList<IColumn>>.Ignored)).Invokes(() =>
			{
				transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, subColumns.ToArray()));
			});

			transformCommand = new ColumnsTransformCommand(new[] { 0, 1 }, transform);
		}

		private void Equivalent(IColumn col, ColumnStruct col2, string msg = "")
		{
			col.State.Should().Be(col2.State, msg);
			col.Cells.Should().Equal(col2.Cells, msg);
		}

		[Fact]
		public void Do_SimpleTransform_SwapOldToNewColumns()
		{
			//A
			Initialize(true);

			//A
			transformCommand.Do(table);

			//A
			table.Should().HaveCount(5, "there are 2 columns from original table, 3 new columns");
			table[0].Should().BeSameAs(originalTable[0]);
			Equivalent(table[1], subColumns[0], "the middle columns are replaced by 3 new columns");
			Equivalent(table[2], subColumns[1], "the middle columns are replaced by 3 new columns");
			Equivalent(table[3], subColumns[2], "the middle columns are replaced by 3 new columns");
			table[4].Should().BeSameAs(originalTable[3]);
		}

		[Fact]
		public void Do_TransformHasConstantSubColumnsAndChanges_Throw()
		{
			//A
			Initialize(true);

			//A
			transformCommand.Do(table);
			A.CallTo(() => transform.SubColumns).Returns(new[] { new ColumnStruct(State.Ok, A.Dummy<IEnumerable<string>>()) });
			Action act = () => transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, new[] { A.Dummy<ColumnStruct>() }));

			//A
			act.Should().Throw<InvalidOperationException>();
		}

		[Fact]
		public void Do_ChangingSubcolumns_AddPlaceholder()
		{
			//A
			Initialize(false);

			//A
			transformCommand.Do(table);

			//A
			table.Should().HaveCount(5 + 1, "there are 2 columns from original table, 3 new columns and one placeholder");
			table[0].Should().BeSameAs(originalTable[0]);
			Equivalent(table[1], subColumns[0], "the middle columns are replaced by 3 new columns");
			Equivalent(table[2], subColumns[1], "the middle columns are replaced by 3 new columns");
			Equivalent(table[3], subColumns[2], "the middle columns are replaced by 3 new columns");
			table[4].Should().BeOfType<PlaceHolderColumn>("placeholder is place after the new columns");
			table[5].Should().BeSameAs(originalTable[3]);
		}

		[Fact]
		public void Do_ChangingSubcolumnsChanges_PlaceNewColumnsBeforePlaceholder()
		{
			//A
			Initialize(false);
			var additionalSubColumns = new[] { new ColumnStruct(State.ImplicitlyRemoved, A.Dummy<IEnumerable<string>>()), new ColumnStruct(State.Ok, A.Dummy<IEnumerable<string>>()), };

			//A
			transformCommand.Do(table);
			A.CallTo(() => transform.SubColumns).Returns(Concat<ColumnStruct>(subColumns, additionalSubColumns));
			transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, additionalSubColumns));

			//A
			table.Should().HaveCount(5 + 2 + 1, "there are 2 columns from original table, 3 subcolumns, 2 additional subcolumns and one placeholder");
			table[0].Should().BeSameAs(originalTable[0]);
			Equivalent(table[1], subColumns[0], "the middle columns are replaced by 3 new columns");
			Equivalent(table[2], subColumns[1], "the middle columns are replaced by 3 new columns");
			Equivalent(table[3], subColumns[2], "the middle columns are replaced by 3 new columns");
			Equivalent(table[4], additionalSubColumns[0], "the middle columns are replaced by 3(and then 2 more) new columns");
			Equivalent(table[5], additionalSubColumns[1], "the middle columns are replaced by 3(and then 2 more) new columns");
			table[6].Should().BeOfType<PlaceHolderColumn>("placeholder is place after all new columns");
			table[7].Should().BeSameAs(originalTable[3]);
		}

		[Fact]
		public void Undo_SimpleCase_RemoveAddedColumns()
		{
			//A
			Initialize(true);

			//A
			transformCommand.Do(table);
			transformCommand.Undo.Do(table);

			//A
			table.Should().Equal(originalTable);
		}

		[Fact]
		public void Undo_WithPlaceholder_RemoveAddedColumns()
		{
			//A
			Initialize(false);

			//A
			transformCommand.Do(table);
			transformCommand.Undo.Do(table);

			//A
			table.Should().Equal(originalTable);
		}

		[Fact]
		public void Undo_WithAdditionalColumns_RemoveAddedColumns()
		{
			//A
			Initialize(false);
			var additionalSubColumns = A.CollectionOfDummy<ColumnStruct>(2);

			//A
			transformCommand.Do(table);
			A.CallTo(() => transform.SubColumns).Returns(Concat<ColumnStruct>(subColumns, additionalSubColumns));
			transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, additionalSubColumns));
			transformCommand.Undo.Do(table);

			//A
			table.Should().Equal(originalTable);
		}

		[Fact]
		public void Undo_WithAdditionalColumnsAllOverTheTable_RemoveAddedColumns()
		{
			//A
			Initialize(false);
			var additionalSubColumns = A.CollectionOfDummy<ColumnStruct>(2);

			//A
			transformCommand.Do(table);
			A.CallTo(() => transform.SubColumns).Returns(Concat<ColumnStruct>(subColumns, additionalSubColumns));
			table.Add(table[5]);
			table.RemoveAt(5);
			transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, additionalSubColumns));
			transformCommand.Undo.Do(table);

			//A
			table.Should().Equal(originalTable);
		}

		[Fact]
		public void Undo_ColumnsChanged_DoNothing()
		{
			//A
			Initialize(false);
			var additionalSubColumns = A.CollectionOfDummy<ColumnStruct>(2);

			//A
			transformCommand.Do(table);
			transformCommand.Undo.Do(table);
			A.CallTo(() => transform.SubColumns).Returns(additionalSubColumns);
			transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, additionalSubColumns));

			//A
			table.Should().Equal(originalTable);
		}

		[Fact]
		public void Redo_SimpleCase_SwapOldToNewColumns()
		{
			//A
			Initialize(true);

			//A
			transformCommand.Do(table);
			transformCommand.Undo.Do(table);
			transformCommand.Undo.Undo.Do(table);

			//A
			table.Should().HaveCount(5, "there are 2 columns from original table, 3 new columns");
			table[0].Should().BeSameAs(originalTable[0]);
			Equivalent(table[1], subColumns[0], "the middle columns are replaced by 3 new columns");
			Equivalent(table[2], subColumns[1], "the middle columns are replaced by 3 new columns");
			Equivalent(table[3], subColumns[2], "the middle columns are replaced by 3 new columns");
			table[4].Should().BeSameAs(originalTable[3]);
		}

		[Fact]
		public void Redo_ChangingSubcolumnsBetweenUndRedo_PlaceNewColumnsBeforePlaceholder()
		{
			//A
			Initialize(false);
			var additionalSubColumns = new[] {
					 new ColumnStruct(State.ImplicitlyRemoved, A.Dummy<IEnumerable<string>>()),
					 new ColumnStruct(State.Ok, A.Dummy<IEnumerable<string>>()),
					 new ColumnStruct(State.ImplicitlyRemoved, A.Dummy<IEnumerable<string>>())};

			//A
			ActCycle(transformCommand, 0);
			ActCycle(transformCommand.Undo, 1);
			ActCycle(transformCommand.Undo.Undo, 2);

			//A
			table.Should().HaveCount(5 + 3 + 1, "there are 2 columns from original table, 3 subcolumns, 3 additional subcolumns and one placeholder");
			table[0].Should().BeSameAs(originalTable[0]);
			Equivalent(table[1], subColumns[0], "the middle columns are replaced by 3 new columns");
			Equivalent(table[2], subColumns[1], "the middle columns are replaced by 3 new columns");
			Equivalent(table[3], subColumns[2], "the middle columns are replaced by 3 new columns");
			Equivalent(table[4], additionalSubColumns[0], "the middle columns are replaced by 3(and then 3 more) new columns");
			Equivalent(table[5], additionalSubColumns[1], "the middle columns are replaced by 3(and then 3 more) new columns");
			Equivalent(table[6], additionalSubColumns[2], "the middle columns are replaced by 3(and then 3 more) new columns");
			table[7].Should().BeOfType<PlaceHolderColumn>("placeholder is place after all new columns");
			table[8].Should().BeSameAs(originalTable[3]);

			void ActCycle(UndoableCollection<IColumn, IReadOnlyColumn>.ICommand command, int additionIndex)
			{
				command.Do(table);
				subColumns.Add(additionalSubColumns[additionIndex]);
				A.CallTo(() => transform.SubColumns).Returns(subColumns);
				transform.ColumnChanged += Raise.With(new ColumnChangedEventArgs(null, new[] { additionalSubColumns[additionIndex] }));
			}
		}
	}
}