using FakeItEasy;
using FluentAssertions;
using System.Collections.Generic;
using TextToColumn.TableCommands;
using Xunit;

namespace TextToColumn.Tests.TableCommands
{
	public class PerRowTransformTest
	{
		[Fact]
		public void Transform_ConstantSubColumns_InitializeSubColumns()
		{
			//A
			var sourceCol = new[] { new EditableColumn(State.Ok, new[] { "1", "2" }), new EditableColumn(State.Ok, new[] { "3", "4" }) };

			var cellTransform = A.Fake<ICellsTransform>();
			A.CallTo(() => cellTransform.IsRepeating).Returns(false);
			A.CallTo(() => cellTransform.SimpleMaxCells).Returns(new[] { State.Ok, State.ImplicitlyRemoved });
			A.CallTo(() => cellTransform.Transform(A<IList<string>>.Ignored))
				.Returns(new[] { ("ok", State.Ok), ("removed", State.ImplicitlyRemoved) });

			//A
			var transform = new PerRowTransform(cellTransform);
			transform.Transform(sourceCol);

			//A
			transform.HasConstantSubColums.Should().BeTrue();
			transform.SubColumns.Should().HaveCount(2);
		}

		[Fact]
		public void GetSubColumnEnumerator_ConstantSubColumns_SetCorrectEnumeratorsInSubColumns()
		{
			//A
			var sourceCol = new[] { new EditableColumn(State.Ok, new[] { "a", "b" }) };

			var cellTransform = A.Fake<ICellsTransform>();
			A.CallTo(() => cellTransform.IsRepeating).Returns(false);
			A.CallTo(() => cellTransform.SimpleMaxCells).Returns(new[] { State.Ok, State.ImplicitlyRemoved });
			A.CallTo(() => cellTransform.Transform(A<IList<string>>.Ignored))
				.ReturnsNextFromSequence(new[]{
						 new[] { ("1", State.Ok), ("2", State.ImplicitlyRemoved) },
						 new[] { ("3", State.Ok), ("4", State.ImplicitlyRemoved) }
				});

			//A
			var transform = new PerRowTransform(cellTransform);
			transform.Transform(sourceCol);

			//A
			transform.SubColumns[0].Should().Equal("1", "3");
			transform.SubColumns[1].Should().Equal("2", "4");
		}

		[Fact]
		public void GetSubColumnEnumerator_VariableSubColumns_CorrectlyAddSubColumns()
		{
			//A
			var sourceCol = new[] { new EditableColumn(State.Ok, new[] { "a", "b" }) };

			var cellTransform = A.Fake<ICellsTransform>();
			A.CallTo(() => cellTransform.IsRepeating).Returns(true);
			A.CallTo(() => cellTransform.SimpleMaxCells).Returns(new[] { State.Ok, State.ImplicitlyRemoved });
			A.CallTo(() => cellTransform.Transform(A<IList<string>>.Ignored))
				.ReturnsNextFromSequence(new[]{
						 new[] { ("1", State.Ok), ("2", State.ImplicitlyRemoved) },
						 new[] { ("3", State.ImplicitlyRemoved), ("4", State.Ok) }
				});

			//A
			var transform = new PerRowTransform(cellTransform);
			transform.Transform(sourceCol);
			using var monitoredSubject = transform.Monitor();
			System.Linq.Enumerable.ToArray(transform.SubColumns[1]);

			//A
			monitoredSubject.Should().Raise("ColumnChanged")
				 .WithSender(transform)
				 .WithArgs<ColumnChangedEventArgs>(x => x.NewColumns[0].Equals(transform.SubColumns[2]));
			transform.SubColumns.Should().HaveCount(3, "new column is added because `State` of `3` is not `Ok`");
		}

		[Fact]
		public void GetSubColumnEnumerator_VariableSubColumns_SetCorrectEnumeratorsInNewSubColumns()
		{
			//A
			var sourceCol = new[] { new EditableColumn(State.Ok, new[] { "a", "b" }) };

			var cellTransform = A.Fake<ICellsTransform>();
			A.CallTo(() => cellTransform.IsRepeating).Returns(true);
			A.CallTo(() => cellTransform.SimpleMaxCells).Returns(new[] { State.Ok, State.ImplicitlyRemoved });
			A.CallTo(() => cellTransform.Transform(A<IList<string>>.Ignored))
				.ReturnsNextFromSequence(new[]{
						 new[] { ("1", State.Ok), ("2", State.ImplicitlyRemoved) },
						 new[] { ("3", State.ImplicitlyRemoved), ("4", State.Ok) }
				});

			//A
			var transform = new PerRowTransform(cellTransform);
			transform.Transform(sourceCol);
			System.Linq.Enumerable.ToArray(transform.SubColumns[1]);

			//A
			transform.SubColumns[0].Should().Equal("1", null);
			transform.SubColumns[1].Should().Equal("2", "3");
			transform.SubColumns[2].Should().Equal(null, "4");
		}

		//todo: test HasConstantSubColums with variable subcolumns
	}
}