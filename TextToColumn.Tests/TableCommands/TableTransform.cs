using FakeItEasy;
using FluentAssertions;
using System;
using System.Collections.Generic;
using TextToColumn.TableCommands;
using Xunit;

namespace TextToColumn.Tests.TableCommands
{
	public class TableTransformTest
	{
		private Action<IList<IColumn>> firstDo;
		private Action<IList<IColumn>> redo;
		private Action<IList<IColumn>> undo;
		private TableTransform transform;

		public TableTransformTest()
		{
			firstDo = A.Fake<Action<IList<IColumn>>>();
			redo = A.Fake<Action<IList<IColumn>>>();
			undo = A.Fake<Action<IList<IColumn>>>();
			transform = new TableTransform(firstDo, undo, redo);
		}

		[Fact]
		public void Do()
		{
			//A
			var table = A.CollectionOfDummy<IList<IColumn>>(3);

			//A
			transform.Do(table[0]);
			transform.Undo.Do(table[1]);
			transform.Do(table[2]);

			//A
			A.CallTo(() => firstDo(table[0])).MustHaveHappenedOnceExactly();
			A.CallTo(() => undo(table[1])).MustHaveHappenedOnceExactly();
			A.CallTo(() => redo(table[2])).MustHaveHappenedOnceExactly();
		}

		[Fact]
		public void Do_CalledMultipleTimes_Fail()
		{
			//A
			var table = A.CollectionOfDummy<IList<IColumn>>(3);

			//A
			transform.Do(table[0]);
			Action act = () => transform.Do(table[2]);

			//A
			A.CallTo(() => firstDo(table[0])).MustHaveHappenedOnceExactly();
			act.Should().Throw<Exception>();
		}

		[Fact]
		public void Undo_CalledBeforeDo_Fail()
		{
			//A
			var table = A.CollectionOfDummy<IList<IColumn>>(3);

			//A
			Action act = () => transform.Undo.Do(table[1]);

			//A
			act.Should().Throw<Exception>();
		}

		[Fact]
		public void Undo_CalledMultipleTimes_Fail()
		{
			//A
			var table = A.CollectionOfDummy<IList<IColumn>>(3);

			//A
			transform.Do(table[0]);
			transform.Undo.Do(table[1]);
			Action act = () => transform.Undo.Do(table[2]);

			//A
			A.CallTo(() => firstDo(table[0])).MustHaveHappenedOnceExactly();
			A.CallTo(() => undo(table[1])).MustHaveHappenedOnceExactly();
			act.Should().Throw<Exception>();
		}
	}
}