using System;
using System.Collections.Generic;

namespace TextToColumn.Tests
{
	static class Utilities
	{
		public static IList<T> Concat<T>(params object[] list)
		{
			List<T> concatanated = new List<T>();
			foreach (var item in list)
			{
				switch (item)
				{
					case T single:
						concatanated.Add(single);
						break;

					case IEnumerable<T> collection:
						concatanated.AddRange(collection);
						break;

					default:
						throw new ArgumentException($"Argument has unknown type: ${item.GetType()}");
				}
			}
			return concatanated;
		}
	}
}