using FakeItEasy;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace TextToColumn.Tests
{
	public class UndoableCollection
	{
		private UndoableCollection<string, object>.ICommand doCommand;
		private UndoableCollection<string, object>.ICommand undoCommand;
		private UndoableCollection<string, object>.ICommand redoCommand;
		private UndoableCollection<string, object> collection;

		public UndoableCollection()
		{
			collection = new UndoableCollection<string, object>();

			doCommand = A.Fake<UndoableCollection<string, object>.ICommand>();
			undoCommand = A.Fake<UndoableCollection<string, object>.ICommand>();
			redoCommand = A.Fake<UndoableCollection<string, object>.ICommand>();
			A.CallTo(() => doCommand.Undo).Returns(undoCommand);
			A.CallTo(() => undoCommand.Undo).Returns(redoCommand);
		}

		[Fact]
		public void Do_Whenever_CallCommand()
		{
			//A
			collection.Do(doCommand);

			//A
			collection.CanRedo.Should().BeFalse();
			collection.CanUndo.Should().BeTrue();
			A.CallTo(() => doCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
		}

		[Fact]
		public void Undo_AfterDo_CallCommand()
		{
			//A
			collection.Do(doCommand);
			collection.Undo();

			//A
			collection.CanRedo.Should().BeTrue();
			collection.CanUndo.Should().BeFalse();
			A.CallTo(() => doCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
			A.CallTo(() => undoCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
		}

		[Fact]
		public void Do_AnotherCommandAfterUndo_CallCommand()
		{
			//A
			var doCommand2 = A.Fake<UndoableCollection<string, object>.ICommand>();

			//A
			collection.Do(doCommand);
			collection.Undo();
			collection.Do(doCommand2);

			//A
			collection.CanRedo.Should().BeFalse();
			collection.CanUndo.Should().BeTrue();
			A.CallTo(() => doCommand2.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
			A.CallTo(() => doCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
			A.CallTo(() => undoCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
		}

		[Fact]
		public void Redo__CallCommand()
		{
			//A
			var doCommand2 = A.Fake<UndoableCollection<string, object>.ICommand>();

			//A
			collection.Do(doCommand2);
			collection.Do(doCommand);
			collection.Undo();
			collection.Redo();

			//A
			collection.CanRedo.Should().BeFalse();
			collection.CanUndo.Should().BeTrue();
			A.CallTo(() => doCommand2.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
			A.CallTo(() => doCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
			A.CallTo(() => undoCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
			A.CallTo(() => redoCommand.Do(A<IList<string>>.Ignored)).MustHaveHappenedOnceExactly();
		}
	}
}