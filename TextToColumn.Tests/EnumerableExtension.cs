using FakeItEasy;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace TextToColumn.Tests
{
	public class EnumerableExtensionTest
	{
		public class IndexOfFirst
		{
			[Fact]
			public void Present_ReturnIndex()
			{
				string[] list = new[] { "0", "1", "2", "3", "4" };
				list.IndexOfFirst(x => x == "2").Should().Be(2, "it is the first index with '2'");
			}

			[Fact]
			public void MultiplePresent_ReturnFirstIndex()
			{
				string[] list = new[] { "0", "1", "2", "3", "2" };
				list.IndexOfFirst(x => x == "2").Should().Be(2, "it is the first index with '2'");
			}

			[Fact]
			public void NotPresent_ReturnNegativeOne()
			{
				string[] list = new[] { "0", "1", "2", "3", "4" };
				list.IndexOfFirst(x => x == "5").Should().Be(-1, "there is no '5' in the list");
			}
		}

		public class InverseEnumerables
		{
			private string[][] matrix;

			public InverseEnumerables()
			{
				matrix = new[] {
					 new[]{ "0,0", "0,1", "0,2" },
					 new[]{ "1,0", "1,1", "1,2" },
					 new[]{ "2,0", "2,1", "2,2" },
					 new[]{ "3,0", "3,1", "3,2" }};
			}

			[Fact]
			public void MatrixLazyEval_InverseMatrix()
			{
				string[][] expectedMatrix = new[] {
					 new[]{ "0,0", "1,0", "2,0", "3,0" },
					 new[]{ "0,1", "1,1", "2,1", "3,1" },
					 new[]{ "0,2", "1,2", "2,2", "3,2" }};

				var inverseMatrix = matrix.InverseEnumerables();

				int i = 0;
				foreach (var item in inverseMatrix)
				{
					expectedMatrix[i].Should().Equal(item, "columns transforms to rows and rows transforms to columns");
					i++;
				}
			}

			[Fact]
			public void Matrix_InverseMatrix()
			{
				string[][] expectedMatrix = new[] {
					 new[]{ "0,0", "1,0", "2,0", "3,0" },
					 new[]{ "0,1", "1,1", "2,1", "3,1" },
					 new[]{ "0,2", "1,2", "2,2", "3,2" } };

				var inverseMatrix = matrix.InverseEnumerables().ToArray();

				int i = 0;
				foreach (var item in inverseMatrix)
				{
					expectedMatrix[i].Should().Equal(item, "columns transforms to rows and rows transforms to columns");
					i++;
				}
			}

			[Fact]
			public void IrregularTableOmitPolicy_InverseTable()
			{
				string[][] matrix = new[] {
					 new[]{ "0,0", "0,1", "0,2", "0,3" },
					 new[]{ "1,0", "1,1", "1,2" },
					 new[]{ "2,0", "2,1", "2,2", "2,3" },
					 new[]{ "3,0", "3,1", "3,2", "3,3" }};
				string[][] expectedMatrix = new[] {
					 new[]{ "0,0", "1,0", "2,0", "3,0" },
					 new[]{ "0,1", "1,1", "2,1", "3,1" },
					 new[]{ "0,2", "1,2", "2,2", "3,2" },
					 new[]{ "0,3", "2,3", "3,3" } };

				var inverseMatrix = matrix.InverseEnumerables(EnumerableExtension.InversePolicy.Omit);

				int i = 0;
				foreach (var item in inverseMatrix)
				{
					expectedMatrix[i].Should().Equal(item, "columns transforms to rows and rows transforms to columns");
					i++;
				}
			}

			[Fact]
			public void IrregularTableFillPolicy_FillToInverseMatrix()
			{
				string[][] matrix = new[] {
					 new[]{ "0,0", "0,1", "0,2", "0,3" },
					 new[]{ "1,0", "1,1", "1,2" },
					 new[]{ "2,0", "2,1", "2,2", "2,3" },
					 new[]{ "3,0", "3,1", "3,2", "3,3" }};
				string[][] expectedMatrix = new[] {
					 new[]{ "0,0", "1,0", "2,0", "3,0" },
					 new[]{ "0,1", "1,1", "2,1", "3,1" },
					 new[]{ "0,2", "1,2", "2,2", "3,2" },
					 new[]{ "0,3", null, "2,3", "3,3" }};

				var inverseMatrix = matrix.InverseEnumerables(EnumerableExtension.InversePolicy.FillWithDefault);

				int i = 0;
				foreach (var item in inverseMatrix)
				{
					expectedMatrix[i].Should().Equal(item, "columns transforms to rows and rows transforms to columns");
					i++;
				}
			}

			[Fact]
			public void MoveNextThrows_RethrowAndDispose()
			{
				int linesCount = 5;
				var matrix = A.CollectionOfFake<IEnumerable<string>>(linesCount);

				var enumerators = A.CollectionOfFake<IEnumerator<string>>(linesCount);
				A.CallTo(() => enumerators[0].MoveNext()).Throws(new InvalidOperationException());
				for (int i = 0; i < matrix.Count; i++)
				{
					A.CallTo(() => matrix[i].GetEnumerator()).Returns(enumerators[i]);
				}

				//A
				var act = new Action(() => matrix.InverseEnumerables().ToArray());

				//A
				act.Should().ThrowExactly<InvalidOperationException>();
				foreach (var enumerator in enumerators)
				{
					A.CallTo(() => enumerator.Dispose()).MustHaveHappenedOnceExactly();
				}
			}

			[Fact]
			public void EnumeratesAll_DisposeEnumerators()
			{
				int linesCount = 5;
				var matrix = A.CollectionOfFake<IEnumerable<string>>(linesCount);
				var enumerators = A.CollectionOfFake<IEnumerator<string>>(linesCount);
				for (int i = 0; i < matrix.Count; i++)
				{
					A.CallTo(() => matrix[i].GetEnumerator()).Returns(enumerators[i]);
				}

				//A
				var inverseMatrix = matrix.InverseEnumerables().ToArray();

				//A
				foreach (var enumerator in enumerators)
				{
					A.CallTo(() => enumerator.Dispose()).MustHaveHappenedOnceExactly();
				}
			}
		}
	}
}