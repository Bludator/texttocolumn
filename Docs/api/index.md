## Design overview

### Table representation

Table is List of columns.
`MainWindow` show `TableVM` - ViewModel of the table.
`TableVM.Table` property is a collection of column ViewModels.
`TableVM` also provides means of table transforms via `ICommand` properties like `TableVM.Split`, `TableVM.Merge` and `TableVM.Remove`
as well as `Undo`/`Redo` functionality.

Following schema show relations between a table (as `TableVM`) and columns (ViewModels as `ColumnVM` and column models as `IColumn` and `IReadOnlyColumn`) as in showing and transforming data

```mermaid
classDiagram
    TableVM o-- ObservableIReadOnlyListVM : Provides collection of ViewModels
    ObservableIReadOnlyListVM o-- UndoableCollection : Reflects changes of models into collection of VMs
    TableVM ..> UndoableCollection  : Uses table directly for table transforms
    ObservableIReadOnlyListVM o-- ColumnVM : Has list of synced ColumnVMs
    UndoableCollection <-- ColumnVM : Uses `UndoableCollection.ICommand` for  cell edits
    UndoableCollection o.. IReadOnlyColumn : Provides immutable columns
    UndoableCollection o-- IColumn : Maintains list of columns
    UndoableCollection ..> UndoableCollection_ICommand : Uses to store transforms
    TableVM ..> UndoableCollection_ICommand : ICommands are converted to `UndoableCollection.ICommand`
    UndoableCollection_ICommand --> IColumn : Works on mutable columns
    IReadOnlyColumn --o ColumnVM
    IColumn <|-- IReadOnlyColumn

    class TableVM{
        UndoableCollection<IColumn, IReadOnlyColumn> Table
        ObservableIReadOnlyListVM<UndoableCollection<IColumn, IReadOnlyColumn>, IReadOnlyColumn, ColumnVM> Columns
        ICommand ...
    }
    class ObservableIReadOnlyListVM~UndoableCollection<IColumn, IReadOnlyColumn>, IReadOnlyColumn, ColumnVM~{
        indexer[int]: IReadOnlyColumn
        IEnumerator<ColumnVM> GetEnumerator()
        NotifyCollectionChangedEventHandler CollectionChanged
    }
    class UndoableCollection~IColumn, IReadOnlyColumn~{
        Do(UndoableCollection_ICommand command) void
        Undo() void
        Redo() void

        indexer[int]: IReadOnlyColumn
        IEnumerator<Column> GetEnumerator()
        NotifyCollectionChangedEventHandler CollectionChanged
    }
    class UndoableCollection_ICommand{
        UndoableCollection_ICommand Undo
        Do(IList<IColumn> table) void
    }
    <<interface>> UndoableCollection_ICommand
    <<interface>> IColumn
    <<interface>> IReadOnlyColumn


```

(Irrelevant properties and functions are skipped for brevity)

### Column representation

Columns are represented as `IEnumerable` of cells.
`ColumnVM` references model through `IReadOnlyColumn` interface, any edits of column or cells are accomplished via `TableVM` reference.

Following schema shows relation between Model and ViewModel of columns:

```mermaid
classDiagram
    IReadOnlyColumn <|-- IColumn
    IReadOnlyColumn o-- ColumnVM
    ColumnStruct o-- EditableColumn : Holds column data
    IColumn <|.. EditableColumn
    EditableColumn .. ColumnVM : through `IReadOnlyColumn`

    class IColumn{
        State State
        GetEdit(int row) string
        SetEdit(int row, string newText) void
    }
    <<interface>> IColumn
    class IReadOnlyColumn{
        State State_getter
        IEnumerable~string~ Cells
    }
    <<interface>> IReadOnlyColumn
    class ColumnStruct{
        State State
        IEnumerable~string~ Cells
    }
    class ColumnVM{
        IReadOnlyColumn Model
        ObservableCollection<string> Cells
        ICommand LoadModer
        ...
    }
```

(In places of `...` irrelevant properties and functions are skipped for brevity)

### Cell representation

Cells are simple `string`

### Lazy loading and transforming

Remarks to the flowchart:

-   Data flows in the opposite direction than the arrows
-   New Enumerable is created in places indicated by `Enumerator.MoveNext()`

```mermaid
graph TD
    user(("User need more rows"))-->table["calls `LoadMore()` on one or more `ColumnVM`s"]

    table--"`LoadMore()`"-->colVM["ColumnVM: <br> calls `Enumerator.MoveNext()` on the model <br> to retrieve desired number of cells"]
    colVM --> col["Enumerating `Column"]

    col --"`Enumerator.MoveNext()`"--> isEdit{"Is cell edited?"}
    isEdit --Yes--> edit[apply edit]
    edit-->colStruct["Enumerating `ColumnStruct`"]
    isEdit --No--> colStruct

    colStruct --"`Enumerator.MoveNext()`"--> isT{Is column transformed?}
    isT --Yes-->t["Enumerating enumerator created and <br> put to the table by `IColumnsTransform` <br> e.g. `PerRowTransform`"]
    isT --No-->source[("loads data from `ISource`")]

    t-->isAdd{"Do the transform yields any more columns?"}
    isAdd--Yes-->add[adds new columns to the table<br> on the place of `PlaceHolderColumn`]
    isAdd--No-->ts["Transform data from the source `Column`s"]
    ts--> col
    add-->ts
    add-->table
```

### Transforms

Relation among various transform related classes:

```mermaid
classDiagram
    UndoableCollection --> UndoableCollection_ICommand : provides an Undoable commands
    UndoableCollection <--> TableExtensions : extends `UndoableCollection`'s functionality
    UndoableCollection_ICommand <|.. TableTransform
    TableExtensions --> TableTransform : the simplest transforms passed by `Action<IList<IColumn>>`
    UndoableCollection_ICommand <|.. ColumnsTransformCommand
    TableExtensions --> ColumnsTransformCommand : uses for more complex transforms
    ColumnsTransformCommand ..> IColumnsTransform : transforms subset of columns to other columns
    ColumnsTransformCommand --> PlaceHolderColumn : creates placeholders in the table
    IColumnsTransform <|.. PerRowTransform
    PerRowTransform --> ICellsTransform : transforms each cell with `ICellsTransform`
    ICellsTransform <|.. Merge
    ICellsTransform <|.. Offset
    ICellsTransform <|.. Regex
    ICellsTransform <|.. Delimiter

    class TableExtensions{
        ...all_table_transform_methods
    }
    class TableTransform{
        TableTransform(Action<IList<IColumn>> firstDo, Action<IList<IColumn>> undo, Action<IList<IColumn>> redo)
    }

    class UndoableCollection~IColumn, IReadOnlyColumn~{
        Do(UndoableCollection_ICommand command) void
        Undo() void
        Redo() void
    }
    class UndoableCollection_ICommand{
        UndoableCollection_ICommand Undo
        Do(IList<IColumn> table) void
    }
    class IColumnsTransform{
        bool HasConstantSubColums
        IList<IColumn> Sources
        IList<ColumnStruct> SubColumns
        Transform(IList<IColumn> Sources) void
    }
    <<interface>> IColumnsTransform
    class ICellsTransform{
        bool IsRepeating
        Transform(IList<string> sourceCells) : (string text, State state)[]
    }
    <<interface>> ICellsTransform
```
