# Text to column

![UI screenshot](/docs/images/screenshot.png)

## Features
- **Undo & Redo**
- Column **splitting**:
	- **offset**: split the selected column after given number of characters
	- **delimiter**: split the selected column on first encounter of given character
	- **regex**: the text in the selected column is processed by the regex. The *capturing groups* specify what will be placed in new columns the rest is removed (placed in removed columns)
	-	all options could be repeated to produce multiple columns.
- **Merge** multiple columns into one.
- **Reorder** columns by drag & drop.
- **Edit** single cell in a column
- **Lazy load** column as text (also drag & drop support)
	- append columns
- Save to CSV

## Technical overview
[see this document](/docs/api/index.md)
