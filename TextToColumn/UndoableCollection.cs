using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TextToColumn
{
	/// <summary>
	///   Collection that provide <see cref="Undo"/>, <see cref="Redo"/> functionality, expose <see cref="T"/> as
	///   <see cref="TReadOnly"/> in collection and expose mutable <see cref="T"/> inside <see cref="UndoableCollection{T, TReadOnly}.ICommand"/>
	/// </summary>
	/// <typeparam name="T"> type exposed just through commands </typeparam>
	/// <typeparam name="TReadOnly"> publicly exposed type </typeparam>
	public class UndoableCollection<T, TReadOnly> : IReadOnlyList<TReadOnly>,
		 INotifyCollectionChanged, IUndoable<UndoableCollection<T, TReadOnly>.ICommand>
		 where T : TReadOnly
	{
		/// <summary>
		///   Wraps undo-able commands on a table
		/// </summary>
		public interface ICommand
		{
			/// <summary>
			///   Undo results of the <see cref="Do(IList{T})"/> couldn't be called before <see cref="Undo"/>
			/// </summary>
			/// <remarks>
			///   IF <see cref="Undo"/>.Undo (= Redo)is implemented as the original object, it must return the table to the
			///   exact state as it was after the first <see cref="Do"/> i.e. all instances of columns must be preserved
			/// </remarks>
			ICommand Undo { get; }

			/// <summary>
			///   Executes command on the table,
			/// </summary>
			/// <param name="table"> </param>
			void Do(IList<T> table);
		}

		private readonly Stack<ICommand> undoStack = new Stack<ICommand>();
		private readonly Stack<ICommand> redoStack = new Stack<ICommand>();

		private readonly ObservableCollection<T> _Columns = new ObservableCollection<T>();

		public event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add
			{
				((INotifyCollectionChanged)_Columns).CollectionChanged += value;
			}

			remove
			{
				((INotifyCollectionChanged)_Columns).CollectionChanged -= value;
			}
		}

		/// <inheritdoc/>
		public bool CanRedo => redoStack.Count > 0;

		/// <inheritdoc/>
		public bool CanUndo => undoStack.Count > 0;

		/// <inheritdoc/>
		public TReadOnly this[int index] => _Columns[index];

		/// <inheritdoc/>
		public int Count => _Columns.Count;

		public UndoableCollection()
		{ }

		/// <inheritdoc/>
		public void Do(ICommand command)
		{
			redoStack.Clear();
			DoCommand(command);
		}

		/// <inheritdoc/>
		public void Undo()
		{
			var command = undoStack.Pop();
			redoStack.Push(command.Undo);
			command.Do(_Columns);
		}

		private void DoCommand(ICommand command)
		{
			command.Do(_Columns);
			undoStack.Push(command.Undo);
		}

		/// <inheritdoc/>
		public void Redo() => DoCommand(redoStack.Pop());

		public IEnumerator<TReadOnly> GetEnumerator() => ((IEnumerable<TReadOnly>)_Columns).GetEnumerator();

		/// <summary>
		///   Returns generic <see cref="GetEnumerator"/>
		/// </summary>
		/// <returns> </returns>
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}