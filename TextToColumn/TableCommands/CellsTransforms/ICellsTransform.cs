using System.Collections.Generic;

namespace TextToColumn
{
	/// <summary>
	///   Represents a transform of multiple cells.
	/// </summary>
	public interface ICellsTransform
	{
		/// <summary>
		///   Maximum number and <see cref="State"/> of cells returned by <see cref="Transform(IList{string})"/> without repeating
		/// </summary>
		State[] SimpleMaxCells { get; }

		/// <summary>
		///   Indicate whether the transform should be repeated after first match.
		/// </summary>
		bool IsRepeating { get; }

		/// <summary>
		///   Method to transform input cells strings to other cells.
		/// </summary>
		/// <remarks> Returning array could skip empty cells but not more than one of a particular <see cref="State"/> </remarks>
		/// <param name="sourceCells"> String cells to be transformed. </param>
		/// <returns> Array of tuples with transformed string and matching <see cref="State"/> </returns>
		/// <seealso cref="CellsTransforms.Merge"/>
		/// <seealso cref="CellsTransforms.Offset"/>
		/// <seealso cref="CellsTransforms.Delimiter"/>
		/// <seealso cref="CellsTransforms.RegexGroups"/>
		(string text, State state)[] Transform(IList<string> sourceCells);
	}
}