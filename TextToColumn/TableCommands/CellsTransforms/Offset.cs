using System;
using System.Collections.Generic;

namespace TextToColumn.CellsTransforms
{
	/// <summary>
	///   Class represents action of splitting string by offset.
	/// </summary>
	public class Offset : ICellsTransform
	{
		/// <summary>
		///   If set to <c> true </c> keep splitting until there is less characters than <see cref="Length"/>
		/// </summary>
		public bool IsRepeating { get; }

		/// <summary>
		///   Position at which the text will be split.
		/// </summary>
		public int Length { get; }

		///<inheritdoc/>
		public State[] SimpleMaxCells => new[] { State.Ok, State.Ok };

		/// <summary>
		///   Constructor, sets <see cref="Length"/> and <see cref="IsRepeating"/>
		/// </summary>
		/// <param name="offset"> See <seealso cref="Length"/> </param>
		/// <param name="isRepeating"> See <see cref="IsRepeating"/> </param>
		/// <exception cref="ArgumentOutOfRangeException"> When assigned value is not greater than 0 </exception>
		public Offset(int offset, bool isRepeating = false)
		{
			if (offset <= 0) throw new ArgumentOutOfRangeException(nameof(offset), offset, "Offset must be greater than 0");
			this.Length = offset;
			this.IsRepeating = isRepeating;
		}

		/// <summary>
		///   Splits the first input cell from <paramref name="sourceCells"/> in position indicated by
		///   <see cref="Length"/> and if <see cref="IsRepeating"/> is set to <c> true </c> the action is repeated.
		/// </summary>
		/// <remarks>
		///   <list type="bullet">
		///     <item>
		///       <description> Doesn't return trailing empty string. </description>
		///     </item>
		///     <item>
		///       <description>
		///         If <see cref="Length"/> is larger than input cell text's length, this method will return original string.
		///       </description>
		///     </item>
		///     <item>
		///       <description>
		///         In repeating mode input cell is split and on the second part this action is done again until there
		///         isn't enough characters left.
		///       </description>
		///     </item>
		///   </list>
		/// </remarks>
		/// <param name="sourceCells"> List of cells where only the first will be split </param>
		/// <returns> Array of tuples with split string and <see cref="State.Ok"/> </returns>
		public (string, State)[] Transform(IList<string> sourceCells)
		{
			System.Diagnostics.Debug.Assert(sourceCells.Count == 1);
			string text = sourceCells[0];
			if (text != null && text.Length > Length)
			{
				if (IsRepeating)
				{
					int cuts = text.Length / Length;
					if (text.Length % Length == 0)
					{//To prevent trailing empty strings
						cuts--;
					}

					(string text, State state)[] columns = new (string, State)[cuts + 1];
					for (int i = 0; i < cuts; i++)
					{
						columns[i].text = text.Substring(i * Length, Length);
					}
					columns[cuts].text = text.Substring(cuts * Length);
					return columns;
				}
				else
				{
					return new[] { (text.SplitOn(Length, out string newStr), State.Ok),
								(newStr, State.Ok) };
				}
			}
			else
			{
				return new[] { (text, State.Ok) };
			}
		}
	}
}