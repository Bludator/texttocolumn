namespace TextToColumn
{
	/// <summary>
	///   Wrapper for extension methods.
	/// </summary>
	internal static class StringExtensions
	{
		/// <summary>
		///   Splits <paramref name="str"/> at <paramref name="position"/>.
		/// </summary>
		/// <remarks> In edge cases returns empty string </remarks>
		/// <param name="str"> </param>
		/// <param name="position"> index where <paramref name="second"/> starts </param>
		/// <param name="second"> second part of the input string </param>
		/// <returns> first part of the input string </returns>
		public static string SplitOn(this string str, int position, out string second)
		{
			second = str.Substring(position);
			return str.Length == position ? str : str.Remove(position);
		}
	}
}