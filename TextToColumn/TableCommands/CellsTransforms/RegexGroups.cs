using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace TextToColumn.CellsTransforms
{
	/// <summary>
	///   Class represents action of splitting string by regex capturing groups.
	/// </summary>
	public class RegexGroups : ICellsTransform
	{
		/// <summary>
		///   Regex pattern, where capturing groups indicates the desired output.
		/// </summary>
		public string Pattern { get; }

		/// <summary>
		///   If set to <c> true </c> searches again on the last result from previous search/split.
		/// </summary>
		public bool IsRepeating { get; }

		///<inheritdoc/>
		public State[] SimpleMaxCells { get; }

		private Regex regex;

		/// <summary>
		///   Constructor, sets <see cref="Pattern"/> and <see cref="IsRepeating"/>
		/// </summary>
		/// <param name="pattern"> See <seealso cref="Pattern"/> </param>
		/// <param name="isRepeating"> See <seealso cref="IsRepeating"/> </param>
		public RegexGroups(string pattern, bool isRepeating = false)
		{
			if (pattern == null) throw new ArgumentNullException(nameof(pattern));
			if (pattern == "") throw new ArgumentException("Regex pattern can not be empty string", nameof(pattern));
			this.Pattern = pattern;
			this.IsRepeating = isRepeating;

			regex = new Regex(pattern);

			List<State> expectedCells = new List<State>();
			foreach (var item in regex.GetGroupNames())
			{
				expectedCells.Add(State.ImplicitlyRemoved);
				expectedCells.Add(State.Ok);
			}
			expectedCells.Add(State.ImplicitlyRemoved);

			SimpleMaxCells = expectedCells.ToArray();
		}

		/// <summary>
		///   Splits the first input cell from <paramref name="sourceCells"/> by regex capturing groups. If
		///   <see cref="IsRepeating"/> is set to <c> true </c> whole regex-search is repeated on the last element from
		///   the previous search.
		/// </summary>
		/// <remarks>
		///   <list type="bullet">
		///     <item>
		///       <description>
		///         String in regex capturing group will be market as <see cref="State.Ok"/>, other parts of the input
		///         will be market as <see cref="State.ImplicitlyRemoved"/>.
		///       </description>
		///     </item>
		///     <item>
		///       <description>
		///         If <see cref="Pattern"/> does not match anything, returns unchanged string from input cell with <see cref="State.ImplicitlyRemoved"/>.
		///       </description>
		///     </item>
		///     <item>
		///       <description>
		///         If <see cref="Pattern"/> contains nested capturing groups, contents of the inner group will be place
		///         after the outer group
		///       </description>
		///     </item>
		///   </list>
		/// </remarks>
		/// <param name="sourceCells"> List of cells where only the first will be split </param>
		/// <returns>
		///   Array of tuples, where elements from capturing group is in tuple with <see cref="State.Ok"/> and others with <see cref="State.ImplicitlyRemoved"/>
		/// </returns>
		public (string, State)[] Transform(IList<string> sourceCells)
		{
			Debug.Assert(sourceCells.Count == 1);
			string text = sourceCells[0];

			if (text == null || !regex.IsMatch(text))
			{
				return new[] { (text, State.ImplicitlyRemoved) };
			}

			List<(string, State)> newColumns = new List<(string, State)>();
			int lastSavedIndex = 0;
			IEnumerable<Match> matches = IsRepeating ?
				 (IEnumerable<Match>)regex.Matches(text) : Enumerable.Repeat(regex.Match(text), 1);

			foreach (var match in matches)
			{
				// 0-th group is whole match, ignore it
				for (int i = 1; i < match.Groups.Count; i++)
				{
					int index = match.Groups[i].Index;
					if (index - lastSavedIndex > 0)
					{
						newColumns.Add((text[lastSavedIndex..index], State.ImplicitlyRemoved));
					}
					newColumns.Add((text.Substring(index, match.Groups[i].Length), State.Ok));

					lastSavedIndex = Math.Max(lastSavedIndex, index + match.Groups[i].Length);
				}
			}
			if (text.Length - lastSavedIndex != 0)
			{
				newColumns.Add((text[lastSavedIndex..], State.ImplicitlyRemoved));
			}
			Debug.Assert(IsRepeating || newColumns.Count <= SimpleMaxCells.Length,
				 "Without repeating `Transform` must return less than `SimpleMaxCells.Length`",
				 $"Expected:\t <={SimpleMaxCells.Length}\n Actual:\t {newColumns.Count}");
			return newColumns.ToArray();
		}
	}
}