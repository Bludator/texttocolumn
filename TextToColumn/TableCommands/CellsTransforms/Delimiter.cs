using System;
using System.Collections.Generic;

namespace TextToColumn.CellsTransforms
{
	/// <summary>
	///   Class represents action of splitting string by delimiter.
	/// </summary>
	public class Delimiter : ICellsTransform
	{
		/// <summary>
		///   If set to <c> true </c> searches for all <see cref="DelimiterStr"/> in string.
		/// </summary>
		public bool IsRepeating { get; }

		/// <summary>
		///   string represents delimiter.
		/// </summary>
		public string DelimiterStr { get; }

		///<inheritdoc/>
		public State[] SimpleMaxCells => new[] { State.Ok, State.ImplicitlyRemoved, State.Ok };

		/// <summary>
		///   Constructor, sets <see cref="DelimiterStr"/> and <see cref="IsRepeating"/>
		/// </summary>
		/// <param name="delimiterStr"> See <see cref="DelimiterStr"/> </param>
		/// <param name="isRepeating"> See <see cref="IsRepeating"/> </param>
		public Delimiter(string delimiterStr, bool isRepeating = false)
		{
			if (delimiterStr == null) throw new ArgumentNullException(nameof(delimiterStr));
			if (delimiterStr == "") throw new ArgumentException("Delimiter can not be empty string", nameof(delimiterStr));
			this.DelimiterStr = delimiterStr;
			this.IsRepeating = isRepeating;
		}

		/// <summary>
		///   Splits the first input cell from <paramref name="sourceCells"/> by <see cref="DelimiterStr"/> and if
		///   <see cref="IsRepeating"/> is set to <c> true </c> the action is repeated.
		/// </summary>
		/// <remarks>
		///   <list type="bullet">
		///     <item>
		///       <description>
		///         Delimiter is not removed from string, it is instead put as standalone element in the outputting array
		///         and marked as <see cref="State.ImplicitlyRemoved"/>.
		///       </description>
		///     </item>
		///     <item>
		///       <description>
		///         Between delimiters is always created element in outputting array, even if there was nothing (then the
		///         element has empty string), the same apply for delimiter and beginning of input cell. On the end empty
		///         strings are omitted.
		///       </description>
		///     </item>
		///     <item>
		///       <description>
		///         If <see cref="DelimiterStr"/> is not present, returns unchanged string from input cell with <see cref="State.Ok"/>
		///       </description>
		///     </item>
		///   </list>
		/// </remarks>
		/// <param name="sourceCells"> List of cells where only the first will be split </param>
		/// <returns>
		///   Array of tuples with split string and matching <see cref="State"/>: <see cref="State.ImplicitlyRemoved"/> is
		///   used for delimiters, <see cref="State.Ok"/> otherwise.
		/// </returns>
		public (string, State)[] Transform(IList<string> sourceCells)
		{
			System.Diagnostics.Debug.Assert(sourceCells.Count == 1);
			string text = sourceCells[0];
			if (text == null)
			{
				return new[] { (text, State.Ok) };
			}

			List<(string, State)> columns = new List<(string, State)>();
			int position = text.IndexOf(DelimiterStr);
			while (position != -1)
			{
				columns.Add((text.SplitOn(position, out text), State.Ok));
				columns.Add((text.SplitOn(DelimiterStr.Length, out text), State.ImplicitlyRemoved));
				if (!IsRepeating)
				{
					break;
				}
				position = text.IndexOf(DelimiterStr);
			}
			if (text != "" || columns.Count == 0)
			{
				columns.Add((text, State.Ok));
			}
			return columns.ToArray();
		}
	}
}