using System;
using System.Collections.Generic;

namespace TextToColumn.CellsTransforms
{
	/// <summary>
	///   Merges two or more cells
	/// </summary>
	public class Merge : ICellsTransform
	{
		/// <summary>
		///   Doesn't make sense in this context - always `false`
		/// </summary>
		public bool IsRepeating => false;

		///<inheritdoc/>
		public State[] SimpleMaxCells => new[] { State.Ok };

		public Merge()
		{
		}

		/* private readonly string delimiter;
		 public Merge(string delimiter)
		 {
			  this.delimiter = delimiter;
			  throw new NotImplementedException();
		 }*/

		/// <summary>
		///   Simple string concat
		/// </summary>
		/// <param name="sourceCells"> </param>
		/// <returns> </returns>
		public (string text, State state)[] Transform(IList<string> sourceCells)
		{
			try
			{
				if (sourceCells.Count > 0)
				{
					return new[] { (string.Concat(sourceCells), State.Ok) };
				}
				else
				{
					return new (string text, State state)[0];
				}
			}
			catch (NullReferenceException e)
			{
				throw new ArgumentNullException("source cells can't be null", e);
			}
		}
	}
}