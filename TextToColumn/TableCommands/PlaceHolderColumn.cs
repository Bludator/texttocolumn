using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace TextToColumn.TableCommands
{
	/// <summary>
	///   Placeholder <see cref="IColumn"/>
	/// </summary>
	/// <remarks> Used to place additional columns in a table </remarks>
	/// <seealso cref="ColumnsTransformCommand"/>
	public class PlaceHolderColumn : IColumn
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		///   Returns <see cref="State.Placeholder"/>, can't be set.
		/// </summary>
		public State State { get => State.Placeholder; set => Debug.Fail($"Writing state to placeholder: '${value}'"); }

		/// <summary>
		///   Transform that this placeholders belong to
		/// </summary>
		public IColumnsTransform ColumnTransform { get; }

		public IEnumerable<string> Cells { get => this; set => Debug.Fail($"Writing cells to placeholder: '${value}'"); }

		/// <summary>
		///   Creates new instance with originating transformation
		/// </summary>
		/// <param name="columnTransform"> </param>
		public PlaceHolderColumn(IColumnsTransform columnTransform) => ColumnTransform = columnTransform;

		public string GetEdit(int row) => null;

		public void SetEdit(int row, string newText) => Debug.Fail($"Editing cells in placeholder: \nrow:'${row}'\nnewText: '${newText}'");

		/// <summary>
		///   Returns null
		/// </summary>
		/// <returns> null </returns>
		public IEnumerator<string> GetEnumerator() => null;

		/// <summary>
		///   Calls generic <see cref="GetEnumerator"/>
		/// </summary>
		/// <returns> </returns>
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}