using System;
using System.Collections.Generic;

namespace TextToColumn.TableCommands
{
	/// <summary>
	///   Provides data about changed columns
	/// </summary>
	public class ColumnChangedEventArgs : EventArgs
	{
		/// <summary>
		///   Removed columns
		/// </summary>
		public IList<ColumnStruct> OldColumns { get; }

		/// <summary>
		///   Added columns
		/// </summary>
		public IList<ColumnStruct> NewColumns { get; }

		/// <summary>
		///   Initialize <see cref="ColumnChangedEventArgs"/> with <see cref="OldColumns"/> and <see cref="NewColumns"/>.
		/// </summary>
		/// <param name="OldColumns"> </param>
		/// <param name="NewColumns"> </param>
		public ColumnChangedEventArgs(IList<ColumnStruct> OldColumns, IList<ColumnStruct> NewColumns)
		{
			this.OldColumns = OldColumns ?? new ColumnStruct[0];
			this.NewColumns = NewColumns ?? new ColumnStruct[0];
		}
	}
}