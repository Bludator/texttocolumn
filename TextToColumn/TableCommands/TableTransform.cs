using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TextToColumn.TableCommands
{
	/// <summary>
	///   Implements <see cref="UndoableCollection{IColumn, IReadOnlyColumn}.ICommand"/> interface using Actions.
	/// </summary>
	/// <remarks>
	///   This class also prevents calls to <see cref="Do"/> and <see cref="Undo"/> out of order - Do, Undo, Do etc. by Debug.Fail().
	/// </remarks>
	class TableTransform : UndoableCollection<IColumn, IReadOnlyColumn>.ICommand
	{
		private readonly Action<IList<IColumn>> doAction;
		private readonly Action<IList<IColumn>> undoAction;
		private readonly Action<IList<IColumn>> redoAction;

		private bool isDone = false;
		private bool isRedo = false;

		/// <inheritdoc/>
		public UndoableCollection<IColumn, IReadOnlyColumn>.ICommand Undo { get; }

		/// <summary>
		///   Initialize <see cref="TableTransform"/> with actions to do on <see cref="Do(IList{IColumn})"/> and <see cref="Undo"/>
		/// </summary>
		/// <param name="firstDo"> First do action </param>
		/// <param name="undo"> Undo action </param>
		/// <param name="redo"> Do actions after the <paramref name="firstDo"/> </param>
		public TableTransform(Action<IList<IColumn>> firstDo, Action<IList<IColumn>> undo, Action<IList<IColumn>> redo)
		{
			this.doAction = firstDo;
			this.undoAction = undo;
			this.redoAction = redo;
			Undo = new TransformUndo(this);
		}

		/// <summary>
		///   <inheritdoc/> Prevents duplicate calls
		/// </summary>
		/// <param name="table"> </param>
		public void Do(IList<IColumn> table)
		{
			if (!isDone)
			{
				if (!isRedo)
				{
					doAction(table);
					isRedo = true;
				}
				else
				{
					redoAction(table);
				}
				isDone = true;
			}
			else
			{
				Debug.Fail("`Do()` is called multiple times");
			}
		}

		class TransformUndo : UndoableCollection<IColumn, IReadOnlyColumn>.ICommand
		{
			private readonly TableTransform tableTransform;

			public UndoableCollection<IColumn, IReadOnlyColumn>.ICommand Undo => tableTransform;

			public TransformUndo(TableTransform tableTransform) => this.tableTransform = tableTransform;

			/// <summary>
			///   <inheritdoc/> Prevents calling undo before do
			/// </summary>
			/// <param name="table"> </param>
			public void Do(IList<IColumn> table)
			{
				if (tableTransform.isDone)
				{
					tableTransform.undoAction(table);
					tableTransform.isDone = false;
				}
				else
				{
					Debug.Fail("`Undo` is called before `Do()`");
				}
			}
		}
	}
}