using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TextToColumn.TableCommands
{
	/// <summary>
	///   Transforms <see cref="IColumn"/> s of <see cref="UndoableCollection{T, TReadOnly}"/> with <see cref="IColumnsTransform"/>
	/// </summary>
	public class ColumnsTransformCommand : UndoableCollection<IColumn, IReadOnlyColumn>.ICommand
	{
		private readonly IList<int> sourceIndexes;

		private readonly IColumnsTransform transform;

		private readonly PlaceHolderColumn placeholder;

		private EventHandler<ColumnChangedEventArgs> onChangedEventHandler;

		/// <summary>
		///   Undo results of the <see cref="Do(IList{IColumn})"/>
		/// </summary>
		public UndoableCollection<IColumn, IReadOnlyColumn>.ICommand Undo { get; }

		/// <summary>
		///   Initialize <see cref="ColumnsTransformCommand"/> with a transform
		/// </summary>
		/// <param name="transform"> </param>
		public ColumnsTransformCommand(IList<int> sources, IColumnsTransform transform)
		{
			this.sourceIndexes = sources;
			this.transform = transform;
			if (!transform.HasConstantSubColums)
			{
				placeholder = new PlaceHolderColumn(transform);
			}
			Undo = new TransformUndo(this);
		}

		private bool isDone = false;
		private bool firstDo = true;

		private List<IColumn> originalState = new List<IColumn>();
		private List<IColumn> newState = new List<IColumn>();

		private int lastTransformSubColumns = 0;

		/// <summary>
		///   Transform table with given <see cref="IColumnsTransform"/>, if the transform has
		///   <see cref="IColumnsTransform.HasConstantSubColums"/> set to true places <see cref="PlaceHolderColumn"/> in
		///   table and watches for changes in <see cref="IColumnsTransform.SubColumns"/>
		/// </summary>
		/// <param name="table"> table to modify </param>
		public void Do(IList<IColumn> table)
		{
			if (!isDone)
			{
				if (firstDo)
				{
					originalState = table.ToList();
					FirstDo(table);
					firstDo = false;

					onChangedEventHandler = (sender, args) => OnChanged(sender, args, table);
					transform.ColumnChanged += onChangedEventHandler;
				}
				else
				{
					transform.ColumnChanged += onChangedEventHandler;

					Sync(table, newState);
					//Add columns that was added after undo but before redo
					if (lastTransformSubColumns < transform.SubColumns.Count)
					{
						PlaceOnPlaceholder(table, transform.SubColumns.Skip(lastTransformSubColumns));
						lastTransformSubColumns = transform.SubColumns.Count;
						Sync(newState, table);
					}
				}
				isDone = true;
			}
			else
			{
				Debug.Fail("`Do()` is called multiple times");
			}
		}

		private void FirstDo(IList<IColumn> table)
		{
			List<IColumn> sources = new List<IColumn>();
			foreach (var item in sourceIndexes)
			{
				sources.Add(table[item]);
			}
			transform.Transform(sources);

			int index = table.IndexOfFirst(col => transform.Sources.Contains(col));
			foreach (var item in transform.Sources)
			{
				table.Remove(item);
			}
			if (!transform.HasConstantSubColums)
			{
				table.Insert(index, placeholder);
			}
			foreach (var col in transform.SubColumns.Reverse())
			{
				EditableColumn newCol = new EditableColumn(col);
				table.Insert(index, newCol);
			}

			Sync(newState, table);
		}

		private void OnChanged(object _, ColumnChangedEventArgs args, IList<IColumn> table)
		{
			if (placeholder == null) throw new InvalidOperationException();
			if (args.NewColumns?.Count > 0)
			{
				PlaceOnPlaceholder(table, args.NewColumns);
			}
		}

		private void PlaceOnPlaceholder(IList<IColumn> table, IEnumerable<ColumnStruct> newSubcolumns)
		{
			int index = table.IndexOf(placeholder);

			foreach (var col in newSubcolumns.Reverse())
			{
				EditableColumn newCol = new EditableColumn(col);
				table.Insert(index, newCol);
				newState.Insert(index, newCol);
			}
		}

		private void Sync<T>(IList<T> target, IList<T> source) where T : class
		{
			int shorter = target.Count < source.Count ? target.Count : source.Count;
			for (int i = 0; i < shorter; i++)
			{
				if (target[i] != source[i])
					target[i] = source[i];
			}
			while (target.Count < source.Count)
			{
				target.Add(source[target.Count]);
			}
			while (target.Count > source.Count)
			{
				target.RemoveAt(target.Count - 1);
			}
		}

		/// <summary>
		///   Wraps Undo command
		/// </summary>
		class TransformUndo : UndoableCollection<IColumn, IReadOnlyColumn>.ICommand
		{
			private readonly ColumnsTransformCommand doCmd;

			public UndoableCollection<IColumn, IReadOnlyColumn>.ICommand Undo => doCmd;

			public TransformUndo(ColumnsTransformCommand doCmd) => this.doCmd = doCmd;

			public void Do(IList<IColumn> table)
			{
				if (doCmd.isDone)
				{
					if (doCmd.onChangedEventHandler != null)
					{
						doCmd.transform.ColumnChanged -= doCmd.onChangedEventHandler;
					}
					doCmd.Sync(table, doCmd.originalState);
					doCmd.isDone = false;
					doCmd.lastTransformSubColumns = doCmd.transform.SubColumns.Count;
				}
				else
				{
					Debug.Fail("`Undo` is called before `Do()`");
				}
			}
		}
	}
}