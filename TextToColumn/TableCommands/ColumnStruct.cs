using System.Collections;
using System.Collections.Generic;

namespace TextToColumn.TableCommands
{
	/// <summary>
	///   Simple immutable value struct that wraps <see cref="State"/> and <see cref="Cells"/>
	/// </summary>
	public readonly struct ColumnStruct : IEnumerable<string>
	{
		/// <summary>
		///   Enumerates through cells of this column
		/// </summary>
		public IEnumerable<string> Cells { get; }

		/// <summary>
		///   The state of the column
		/// </summary>
		public State State { get; }

		/// <summary>
		///   Initialize Column
		/// </summary>
		/// <param name="state"> state of the column </param>
		/// <param name="enumerable"> column's cells </param>
		public ColumnStruct(State state, IEnumerable<string> enumerable)
		{
			State = state;
			this.Cells = enumerable;
		}

		/// <summary>
		///   Enumerates through column's rows
		/// </summary>
		/// Enables lazy evaluation
		/// <returns> Returns cell contents in each row </returns>
		public IEnumerator<string> GetEnumerator() => Cells.GetEnumerator();

		/// <summary>
		///   Returns generic <see cref="GetEnumerator"/>
		/// </summary>
		/// <returns> </returns>
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}