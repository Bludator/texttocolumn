using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using TextToColumn.CellsTransforms;
using TextToColumn.TableCommands;

namespace TextToColumn.ViewModel
{
	/// <summary>
	///   Represents table of <see cref="IColumn"/>.
	/// </summary>
	public class TableVM : INotifyPropertyChanged
	{
		/// <summary>
		///   Fires when property is changed to enable binding
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		public readonly UndoableCollection<IColumn, IReadOnlyColumn> Table = new UndoableCollection<IColumn, IReadOnlyColumn>();

		#region Properties

		/// <summary>
		///   Columns of the table
		/// </summary>
		public ObservableIReadOnlyListVM<UndoableCollection<IColumn, IReadOnlyColumn>, IReadOnlyColumn, ColumnVM> Columns { get; }

		/// <summary>
		///   Setup for <see cref="Split"/>
		/// </summary>
		public SplitingOptions SplitingOptions { get; }

		private bool _ShowRemoved = false;
		/// <summary>
		///   If set to <c> true </c> removed columns are visible
		/// </summary>
		public bool ShowRemoved
		{
			get => _ShowRemoved;
			set
			{
				_ShowRemoved = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ShowRemoved)));
			}
		}

		private ColumnVM[] _SelectedColumns = new ColumnVM[0];

		/// <summary>
		///   Selected column from <see cref="Columns"/>.
		/// </summary>
		public ColumnVM[] SelectedColumns
		{
			get => _SelectedColumns;
			set
			{
				_SelectedColumns = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedColumns)));
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedIndexes)));
			}
		}

		/// <summary>
		///   Indexes of <see cref="SelectedColumns"/>
		/// </summary>
		public IList<int> SelectedIndexes
		{
			get
			{
				List<int> indexes = new List<int>();
				foreach (var item in SelectedColumns)
				{
					indexes.Add(Columns.IndexOf(item));
				}
				return indexes;
			}
			set
			{
				List<ColumnVM> columns = new List<ColumnVM>();
				foreach (var item in value)
				{
					columns.Add(Columns[item]);
				}
				SelectedColumns = columns.ToArray();
			}
		}

		#endregion Properties

		#region Commands

		/// <summary>
		///   Command to load new column.
		/// </summary>
		public ICommand Load { get; }

		/// <summary>
		///   Command to save <see cref="TableVM"/>
		/// </summary>
		public ICommand Save { get; }

		/// <summary>
		///   Split <see cref="SelectedColumns"/> using <see cref="SplitingOptions"/>. This command doesn't support
		///   multiple selected columns, it that case <see cref="ICommand.CanExecute(object)"/> returns `false`
		/// </summary>
		public ICommand Split { get; }

		/// <summary>
		///   Merge two or more columns
		/// </summary>
		public ICommand Merge { get; }

		/// <summary>
		///   Command to remove <see cref="SelectedColumns"/>.
		/// </summary>
		public ICommand Remove { get; }

		/// <summary>
		///   Undo last action
		/// </summary>
		public ICommand Undo { get; }

		/// <summary>
		///   Redo last undone action
		/// </summary>
		public ICommand Redo { get; }

		#endregion Commands

		private readonly Func<IList<IEnumerable<string>>> OpenFunction;
		private readonly Action<TableVM> SaveFunction;

		/// <summary>
		///   Initialize table.
		/// </summary>
		/// <param name="OpenFunction"> Function returning list of column to add </param>
		/// <param name="SaveFunction"> Saves the <see cref="TableVM"/> </param>
		public TableVM(Func<IList<IEnumerable<string>>> OpenFunction, Action<TableVM> SaveFunction)
		{
			Columns = new ObservableIReadOnlyListVM<UndoableCollection<IColumn, IReadOnlyColumn>, IReadOnlyColumn, ColumnVM>(Table, m => new ColumnVM(this, m));
			Columns.CollectionChanged += Columns_CollectionChanged;

			this.OpenFunction = OpenFunction;
			this.SaveFunction = SaveFunction;
			SplitingOptions = new SplitingOptions(this);

			//Commands
			Load = new Command(() =>
			{
				var newColumns = OpenFunction();
				if (newColumns != null) Table.AddSources(newColumns);
			});
			Save = new Command(() => SaveFunction(this));

			Split = new Command(OnSplit, _ => SelectedColumns.Length == 1 && !SplitingOptions.HasErrors && SplitingOptions.HasValue());
			Merge = new Command(() => Table.Merge(SelectedIndexes), () => SelectedColumns.Length > 0);
			Remove = new Command(() => Table.Remove(SelectedIndexes), () => SelectedColumns.Length > 0);

			Undo = new Command(Table.Undo, () => Table.CanUndo);
			Redo = new Command(Table.Redo, () => Table.CanRedo);
		}

		private void Columns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					SelectedColumns = e.NewItems.Cast<ColumnVM>().ToArray();
					break;

				case NotifyCollectionChangedAction.Move:
					SelectedColumns = e.NewItems.Cast<ColumnVM>().ToArray();
					break;

				case NotifyCollectionChangedAction.Remove:
					SelectedColumns = new ColumnVM[] { };
					break;

				case NotifyCollectionChangedAction.Replace:
					SelectedColumns = e.NewItems.Cast<ColumnVM>().ToArray();
					break;

				case NotifyCollectionChangedAction.Reset:
					SelectedColumns = new ColumnVM[] { };
					break;

				default:
					break;
			}
		}

		/// <summary>
		///   Move all <see cref="SelectedColumns"/> to index
		/// </summary>
		/// <param name="index"> index to place the columns </param>
		public void Move(int index)
		{
			Table.Move(SelectedIndexes, index);
		}

		/// <summary>
		///   Appends specified column
		/// </summary>
		/// <param name="columnVM"> </param>
		public void Append(ColumnVM columnVM)
		{
			var newColumns = OpenFunction();
			if (newColumns != null) Table.AppendSource(Columns.IndexOf(columnVM), newColumns[0]);
		}

		/// <summary>
		/// </summary>
		/// <param name="_"> </param>
		private void OnSplit(object _)
		{
			System.Diagnostics.Debug.Assert(SelectedColumns.Length == 1);

			if (SplitingOptions.IsOffset)
			{
				Table.Split(SelectedIndexes, new Offset(int.Parse(SplitingOptions.Input), SplitingOptions.IsRepeating));
			}
			else if (SplitingOptions.IsDelimiter)
			{
				Table.Split(SelectedIndexes, new Delimiter(SplitingOptions.Input, SplitingOptions.IsRepeating));
			}
			else if (SplitingOptions.IsRegex)
			{
				Table.Split(SelectedIndexes, new RegexGroups(SplitingOptions.Input, SplitingOptions.IsRepeating));
			}
			else
			{
				return;
			}
			SplitingOptions.Reset();
		}
	}
}