using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace TextToColumn.ViewModel
{
	/// <summary>
	///   Observable collection of ViewModels that reflects any changed to the underlying <see cref="TCollection"/> of Models
	/// </summary>
	/// <remarks>
	///   Despite the <see cref="IReadOnlyList{T}"/> constrain on <see cref="TCollection"/> this class reflect changes of
	///   <see cref="TCollection"/> from outside as long as the <see cref="INotifyCollectionChanged"/> is properly implemented.
	/// </remarks>
	/// <typeparam name="TCollection"> list of Models </typeparam>
	/// <typeparam name="TModel"> type of Model </typeparam>
	/// <typeparam name="TVM"> type of ViewModel </typeparam>
	public class ObservableIReadOnlyListVM<TCollection, TModel, TVM> : IReadOnlyList<TVM>, INotifyCollectionChanged
		 where TCollection : IReadOnlyList<TModel>, INotifyCollectionChanged
		 where TVM : IViewModel<TModel>
	{
		/// <summary>
		///   Collection of models to reflect
		/// </summary>
		protected readonly TCollection modelList;

		/// <summary>
		///   Function to initialize ViewModel from associated Model
		/// </summary>
		protected readonly Func<TModel, TVM> vmInit;

		/// <summary>
		///   Collection of ViewModels
		/// </summary>
		protected readonly List<TVM> vmList = new List<TVM>();

		///<inheritdoc/>
		public event NotifyCollectionChangedEventHandler CollectionChanged;

		/// <summary>
		///   Initialize <see cref="ObservableIReadOnlyListVM"/>
		/// </summary>
		/// <param name="modelList"> list of models </param>
		/// <param name="vmInit"> function to initialize ViewModel from associated Model </param>
		public ObservableIReadOnlyListVM(TCollection modelList, Func<TModel, TVM> vmInit)
		{
			this.modelList = modelList;
			this.vmInit = vmInit;
			modelList.CollectionChanged += ModelListChanged;
			Repopulate();
		}

		///<inheritdoc/>
		public TVM this[int index] => ((IList<TVM>)vmList)[index];

		///<inheritdoc/>
		public int Count => ((ICollection<TVM>)vmList).Count;

		///<inheritdoc/>
		public int IndexOf(TVM item) => vmList.IndexOf(item);

		///<inheritdoc/>
		public IEnumerator<TVM> GetEnumerator() => ((IEnumerable<TVM>)vmList).GetEnumerator();

		///<inheritdoc/>
		IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)vmList).GetEnumerator();

		#region Model synchronization

		private void Repopulate()
		{
			if (vmList.Count != 0)
			{
				vmList.Clear();

				CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
			}
			foreach (var model in modelList)
			{
				vmList.Add(vmInit(model));
			}
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, vmList.ToArray(), 0));
		}

		private void ModelListChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			try
			{
				switch (e.Action)
				{
					case NotifyCollectionChangedAction.Add:
						OnAdd(e);
						break;

					case NotifyCollectionChangedAction.Move:
						OnMove(e);
						break;

					case NotifyCollectionChangedAction.Remove:
						OnRemove(e);
						break;

					case NotifyCollectionChangedAction.Replace:
						OnReplace(e);
						break;

					case NotifyCollectionChangedAction.Reset:
						vmList.Clear();
						CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(e.Action));
						break;

					default:
						System.Diagnostics.Debug.Fail("Unknown action");
						break;
				}
			}
			catch
			{
				System.Diagnostics.Debug.Fail("Something went wrong");
				Repopulate();
			}
		}

		private int ZeroBased(int x) => x >= 0 ? x : 0;

		private void OnAdd(NotifyCollectionChangedEventArgs e)
		{
			List<TVM> newVM = new List<TVM>();
			int changed = e.NewItems.Count;
			for (int i = ZeroBased(e.NewStartingIndex); i < modelList.Count; i++)
			{
				if (vmList.Count <= i || !ReferenceEquals(modelList[i], vmList[i].Model))
				{
					TVM newVieWModel = vmInit(modelList[i]);
					newVM.Add(newVieWModel);
					vmList.Insert(i, newVieWModel);
					changed--;
					if (changed == 0) break;
				}
			}
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, newVM, e.NewStartingIndex));
		}

		private void OnMove(NotifyCollectionChangedEventArgs e)
		{
			List<TVM> newVM = new List<TVM>();

			int changed = e.NewItems.Count;
			for (int i = ZeroBased(e.NewStartingIndex); i < modelList.Count; i++)
			{//todo: implement move handler
				throw new NotImplementedException();
				if (!ReferenceEquals(modelList[i], vmList[i].Model))
				{
					TVM different = vmInit(modelList[i]);
					newVM.Add(different);
					//vmList. = different;
					changed--;
					if (changed == 0) break;
				}
			}
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, newVM, e.NewStartingIndex, e.OldStartingIndex));
		}

		private void OnRemove(NotifyCollectionChangedEventArgs e)
		{
			List<TVM> oldVM = new List<TVM>();
			int changed = e.OldItems.Count;
			for (int i = ZeroBased(e.OldStartingIndex); i < vmList.Count; i++)
			{
				if (modelList.Count <= i || !ReferenceEquals(modelList[i], vmList[i].Model))
				{
					TVM old = vmList[i];
					oldVM.Add(old);
					vmList.RemoveAt(i);
					changed--;
					if (e.OldItems.Count == 0) break;
				}
			}
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, oldVM, e.OldStartingIndex));
		}

		private void OnReplace(NotifyCollectionChangedEventArgs e)
		{
			List<TVM> oldVM = new List<TVM>();
			List<TVM> newVM = new List<TVM>();
			int changed = e.NewItems.Count;

			for (int i = ZeroBased(e.NewStartingIndex); i < modelList.Count; i++)
			{
				if (!ReferenceEquals(modelList[i], vmList[i].Model))
				{
					TVM different = vmInit(modelList[i]);
					newVM.Add(different);
					oldVM.Add(vmList[i]);
					vmList[i] = different;
					changed--;
					if (e.NewItems.Count == 0) break;
				}
			}
			CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, newVM, oldVM, e.NewStartingIndex));
		}

		#endregion Model synchronization
	}
}