using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace TextToColumn.ViewModel
{
	/// <summary>
	///   Extends <see cref="ObservableIReadOnlyListVM{TCollection, TModel, TVM}"/> with write operations.
	/// </summary>
	/// <typeparam name="TCollection"> list of Models </typeparam>
	/// <typeparam name="TModel"> type of Model </typeparam>
	/// <typeparam name="TVM"> type of ViewModel </typeparam>
	public class ObservableIListVM<TCollection, TModel, TVM> : ObservableIReadOnlyListVM<TCollection, TModel, TVM>, IList<TVM>
		 where TCollection : IList<TModel>, IReadOnlyList<TModel>, INotifyCollectionChanged
		 where TVM : IViewModel<TModel>
	{
		///<inheritdoc/>
		TVM IList<TVM>.this[int index] { get => ((IList<TVM>)vmList)[index]; set => ((IList<TModel>)modelList)[index] = value.Model; }

		///<inheritdoc/>
		public bool IsReadOnly => ((ICollection<TModel>)modelList).IsReadOnly;

		///<inheritdoc/>
		public void Add(TVM item) => ((ICollection<TModel>)modelList).Add(item.Model);

		///<inheritdoc/>
		public void Clear() => ((ICollection<TModel>)modelList).Clear();

		///<inheritdoc/>
		public bool Contains(TVM item) => ((ICollection<TVM>)vmList).Contains(item);

		///<inheritdoc/>
		public void CopyTo(TVM[] array, int arrayIndex) => ((ICollection<TVM>)vmList).CopyTo(array, arrayIndex);

		///<inheritdoc/>
		public void Insert(int index, TVM item) => ((IList<TModel>)modelList).Insert(index, item.Model);

		///<inheritdoc/>
		public bool Remove(TVM item) => ((ICollection<TModel>)modelList).Remove(item.Model);

		///<inheritdoc/>
		public void RemoveAt(int index) => ((IList<TModel>)modelList).RemoveAt(index);

		public ObservableIListVM(TCollection modelList, Func<TModel, TVM> vmInit) : base(modelList, vmInit)
		{
		}
	}
}