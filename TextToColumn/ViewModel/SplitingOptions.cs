using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace TextToColumn.ViewModel
{
	/// <summary>
	///   Combines all splitting options
	/// </summary>
	public class SplitingOptions : INotifyPropertyChanged, INotifyDataErrorInfo
	{
		///<inheritdoc/>
		public event PropertyChangedEventHandler PropertyChanged;

		///<inheritdoc/>
		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

		// Create the OnPropertyChanged method to raise the event The calling member's name will be used as the parameter.
		private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string name = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
			Validate();
		}

		private string _Input;
		/// <summary>
		///   input for <see cref="Input"/>
		/// </summary>
		public string Input
		{
			get => _Input;
			set
			{
				_Input = value;
				OnPropertyChanged();
			}
		}

		private bool _IsRepeating;
		/// <summary>
		///   Sets <see cref="ICellsTransform.IsRepeating"/> to <c> true </c>.
		/// </summary>
		public bool IsRepeating
		{
			get => _IsRepeating;
			set
			{
				_IsRepeating = value;
				OnPropertyChanged();
			}
		}

		private bool _IsOffset;
		/// <summary>
		///   Splits by <see cref="CellsTransforms.Offset"/>
		/// </summary>
		public bool IsOffset
		{
			get => _IsOffset;
			set
			{
				_IsOffset = value;
				OnPropertyChanged();
			}
		}

		private bool _IsDelimiter;
		/// <summary>
		///   Splits by <see cref="CellsTransforms.Delimiter"/>
		/// </summary>
		public bool IsDelimiter
		{
			get => _IsDelimiter;
			set
			{
				_IsDelimiter = value;
				OnPropertyChanged();
			}
		}

		private bool _IsRegex;
		/// <summary>
		///   Splits by <see cref="CellsTransforms.RegexGroups"/>
		/// </summary>
		public bool IsRegex
		{
			get => _IsRegex;
			set
			{
				_IsRegex = value;
				OnPropertyChanged();
			}
		}

		private readonly TableVM tableVM;

		/// <summary>
		///   Splits columns in table
		/// </summary>
		public ICommand Split => tableVM.Split;

		private bool _HasErrors = false;
		/// <summary>
		///   indicate if <see cref="Input"/> is valid
		/// </summary>
		public bool HasErrors

		{
			get => _HasErrors;
			set
			{
				_HasErrors = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasErrors)));
			}
		}

		public SplitingOptions(TableVM tableVM)
		{
			Reset();
			this.tableVM = tableVM;
		}

		/// <summary>
		///   Resets options
		/// </summary>
		public void Reset()
		{
			Input = "";
			IsRepeating = false;
			//IsOffset = false;
			//IsDelimiter = false;
			//IsRegex = false;
		}

		/// <summary>
		///   returns true if this class is initialized and has input
		/// </summary>
		/// <returns> </returns>
		public bool HasValue()
		{
			return Input != null && Input != "" &&
				 (IsDelimiter || IsOffset || IsRegex);
		}

		/// <summary>
		///   Validate <see cref="Input"/>
		/// </summary>
		public void Validate()
		{
			bool newHasErrors = false;
			if (IsOffset && !(int.TryParse(Input, out var number) && number > 0))
			{
				newHasErrors = true;
			}
			if (newHasErrors != HasErrors)
			{
				HasErrors = newHasErrors;
				ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(nameof(Input)));
			}
		}

		public IEnumerable GetErrors(string propertyName)
		{
			if (HasErrors && propertyName == nameof(Input))
			{
				return new List<string>() { "Offset must be positive integer" };
			}
			return null;
		}
	}
}