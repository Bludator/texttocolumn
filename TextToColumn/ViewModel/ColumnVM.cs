using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using TextToColumn.TableCommands;

namespace TextToColumn.ViewModel
{
	/// <summary>
	///   ViewModel for <see cref="IReadOnlyColumn"/>
	/// </summary>
	/// <seealso cref="IColumnsTransform"/>
	public class ColumnVM : IEnumerable<string>, IViewModel<IReadOnlyColumn>, INotifyPropertyChanged, IDisposable
	{
		private readonly TableVM tableVM;

		private readonly IEnumerator<string> enumerator;

		private bool isEnumerated = false;
		private bool _DisposedValue;

		///<inheritdoc/>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		///   Cells of the column
		/// </summary>
		public ObservableCollection<CellVM> Cells { get; } = new ObservableCollection<CellVM>();

		/// <summary>
		///   Model of the column
		/// </summary>
		public IReadOnlyColumn Model { get; }

		/// <summary>
		///   State of the column
		/// </summary>
		public State State => Model.State;

		/// <summary>
		///   Loads more cells of the column
		/// </summary>
		public ICommand LoadMore { get; }

		/// <summary>
		///   Appends column
		/// </summary>
		public ICommand Append { get; }

		/// <summary>
		///   Initialize column ViewModel with <paramref name="column"/> model.
		/// </summary>
		/// <param name="column"> model for which is this ViewModel </param>
		public ColumnVM(TableVM table, IReadOnlyColumn column)
		{
			this.tableVM = table;
			Model = column;
			Append = new Command(() => table.Append(this));
			enumerator = Model.GetEnumerator();
			Model.PropertyChanged += Model_PropertyChanged;

			LoadMore = new Command(GetCells, _ => !isEnumerated, nameof(Model.Cells), e => Model.PropertyChanged += e);
		}

		private const int loadSize = 30;

		/// <summary>
		///   Loads next bulk of cells
		/// </summary>
		/// <param name="_"> </param>
		public void GetCells(object _)
		{
			if (!isEnumerated)
			{
				for (int i = 0; i < loadSize; i++)
				{
					if (enumerator.MoveNext())
					{
						var cell = new CellVM(enumerator.Current, Cells.Count);
						cell.PropertyChanged += Cell_PropertyChanged;
						Cells.Add(cell);
					}
					else
					{
						isEnumerated = true;
						break;
					}
				}
			}
		}

		private void Cell_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(CellVM.Value))
			{
				CellVM cellVM = (CellVM)sender;
				tableVM.Table.Edit(tableVM.Columns.IndexOf(this), cellVM.Index, cellVM.Value);
			}
		}

		///<inheritdoc/>
		public IEnumerator<string> GetEnumerator() => Model.GetEnumerator();

		///<inheritdoc/>
		IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)Model).GetEnumerator();

		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(Model.State)) PropertyChanged?.Invoke(this, e);
			else if (e.PropertyName == nameof(Model.Cells)) Cells.Clear();
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_DisposedValue)
			{
				if (disposing)
				{
					enumerator.Dispose();
				}
				_DisposedValue = true;
			}
		}

		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}
	}
}