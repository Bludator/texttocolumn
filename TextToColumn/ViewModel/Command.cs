using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace TextToColumn.ViewModel
{
	/// <summary>
	///   Generic command
	/// </summary>
	public class Command : ICommand, IDisposable
	{
		private readonly Action<object> doThis;
		private readonly Func<object, bool> when;

		///<inheritdoc/>
		public virtual event EventHandler CanExecuteChanged;

		#region Constructors

		/// <summary>
		///   Command , always enabled
		/// </summary>
		/// <param name="on"> Executed on check </param>
		public Command(Action on) : this((x) => on(), (x) => true) { }

		/// <summary>
		///   Command and CanExecute
		/// </summary>
		/// <param name="on"> Executed on check </param>
		/// <param name="when"> </param>
		public Command(Action on, Func<bool> when) : this((x) => on(), (x) => when()) { }

		/// <summary>
		///   Command with arguments, CanExecute with arguments
		/// </summary>
		/// <param name="on"> </param>
		/// <param name="when"> </param>
		public Command(Action<object> on, Func<object, bool> when)
		{
			this.doThis = on;
			this.when = when;
			commands.Add(new WeakReference<Command>(this));
		}

		private readonly PropertyChangedEventHandler propertyChanged;
		private readonly Action<PropertyChangedEventHandler> unsubscribe;

		/// <summary>
		/// </summary>
		/// <param name="on"> </param>
		/// <param name="when"> </param>
		/// <param name="propertyName"> property name to observe on event provided in <paramref name="subscribe"/> </param>
		/// <param name="subscribe"> subscribe listener to PropertyChanged event </param>
		/// <param name="unsubscribe"> unsubscribe listener to PropertyChanged event </param>
		public Command(Action<object> on, Func<object, bool> when,
			 string propertyName,
			 Action<PropertyChangedEventHandler> subscribe,
			 Action<PropertyChangedEventHandler> unsubscribe = null) : this(on, when)
		{
			propertyChanged = (object b, PropertyChangedEventArgs a) => { if (a.PropertyName == propertyName) RaiseCanExecuteChanged(); };
			subscribe(propertyChanged);
			this.unsubscribe = unsubscribe;
		}

		/// <summary>
		/// </summary>
		/// <param name="on"> </param>
		/// <param name="when"> </param>
		/// <param name="propertyNames"> array of property names to observe on event provided in <paramref name="subscribe"/> </param>
		/// <param name="subscribe"> subscribe listener to PropertyChanged event </param>
		/// <param name="unsubscribe"> unsubscribe listener to PropertyChanged event </param>
		public Command(Action<object> on, Func<object, bool> when,
			 string[] propertyNames,
			 Action<PropertyChangedEventHandler> subscribe,
			 Action<PropertyChangedEventHandler> unsubscribe = null) : this(on, when)
		{
			propertyChanged = (object b, PropertyChangedEventArgs a) => { if (Array.IndexOf(propertyNames, a.PropertyName) > -1) RaiseCanExecuteChanged(); };
			subscribe(propertyChanged);
			this.unsubscribe = unsubscribe;
		}

		#endregion Constructors

		///<inheritdoc/>
		public bool CanExecute(object parameter) => when(parameter);

		///<inheritdoc/>
		public void Execute(object parameter) => doThis(parameter);

		/// <summary>
		/// </summary>
		public void RaiseCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);

		private static readonly List<WeakReference<Command>> commands = new List<WeakReference<Command>>();

		public static void RaiseAllCanExecuteChanged()
		{
			foreach (var item in commands)
			{
				if (item.TryGetTarget(out Command command))
				{
					command.RaiseCanExecuteChanged();
				}
			}
		}

		/// <summary>
		///   Unsubscribe all events registered by
		///   <see cref="Command.Command(Action{object}, Func{object, bool}, string, Action{PropertyChangedEventHandler}, Action{PropertyChangedEventHandler})"/>
		///   or <see cref="Command.Command(Action{object}, Func{object, bool}, string[], Action{PropertyChangedEventHandler}, Action{PropertyChangedEventHandler})"/>
		/// </summary>
		public void Unsubscribe() => unsubscribe?.Invoke(propertyChanged);

		/// <summary>
		///   Calls <see cref="Unsubscribe"/>
		/// </summary>
		public void Dispose() => Unsubscribe();
	}
}