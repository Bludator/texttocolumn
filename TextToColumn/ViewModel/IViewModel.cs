namespace TextToColumn.ViewModel
{
	/// <summary>
	///   Specify Model - ViewModel relation
	/// </summary>
	/// <typeparam name="TModel"> </typeparam>
	public interface IViewModel<TModel>
	{
		/// <summary>
		///   Model of the ViewModel
		/// </summary>
		TModel Model { get; }
	}
}