using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TextToColumn.ViewModel
{
	public struct CellVM : INotifyPropertyChanged
	{
		private bool ChangeProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
		{
			if (EqualityComparer<T>.Default.Equals(field, value)) return false;
			field = value;
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			return true;
		}

		private string _Value;
		internal readonly int Index;

		public event PropertyChangedEventHandler PropertyChanged;

		public string Value { get => _Value; set { ChangeProperty(ref _Value, value); } }

		public CellVM(string value, int index)
		{
			PropertyChanged = null;
			_Value = value;
			Index = index;
		}
	}
}