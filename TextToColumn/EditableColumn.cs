using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TextToColumn.TableCommands;

namespace TextToColumn
{
	/// <summary>
	///   Represents an editable column with string rows.
	/// </summary>
	public class EditableColumn : IColumn, INotifyPropertyChanged
	{
		private readonly IEnumerable<string> source;
		private readonly Dictionary<int, string> edits = new Dictionary<int, string>(0);
		private ColumnStruct columnStruct;

		public EditableColumn(State state, IEnumerable<string> enumerable) : this(new ColumnStruct(state, enumerable))
		{
		}

		public EditableColumn(ColumnStruct columnStruct)
		{
			source = columnStruct.Cells;
			this.columnStruct = new ColumnStruct(columnStruct.State, GetEditedCells());
		}

		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		///   The state of the column
		/// </summary>
		public State State
		{
			get => columnStruct.State;
			set
			{
				columnStruct = new ColumnStruct(value, columnStruct.Cells);
				OnPropertyChanged();
			}
		}

		/// <summary>
		///   The cells of the column
		/// </summary>
		public IEnumerable<string> Cells
		{
			get => columnStruct.Cells;
			protected set
			{
				columnStruct = new ColumnStruct(columnStruct.State, value);
				OnPropertyChanged();
			}
		}

		/// <summary>
		///   Enumerates through column's rows.
		/// </summary>
		/// Enables lazy evaluation.
		/// <returns> Returns cell contents in each row </returns>
		public IEnumerator<string> GetEnumerator() => Cells.GetEnumerator();

		/// <summary>
		///   Edit cell by storing the newText
		/// </summary>
		/// <param name="row"> row to edit </param>
		/// <param name="newText"> new text to store in cell or null to reset to original </param>
		public void SetEdit(int row, string newText)
		{
			if (edits.ContainsKey(row))
			{
				if (newText == null)
				{
					edits.Remove(row);
				}
				else
				{
					edits[row] = newText;
				}
			}
			else
			{
				edits.Add(row, newText);
			}
			OnPropertyChanged("Edits");//todo:
		}

		/// <summary>
		///   Get stored edit of the cell
		/// </summary>
		/// <param name="row"> desired row </param>
		/// <returns> stored edit </returns>
		public string GetEdit(int row) => edits.TryGetValue(row, out string value) ? value : null;

		/// <summary>
		///   Returns generic <see cref="GetEnumerator"/>
		/// </summary>
		/// <returns> </returns>
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string name = null)
																			 => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

		private IEnumerable<string> GetEditedCells()
		{
			int index = 0;
			foreach (var item in source)
			{
				if (edits.ContainsKey(index))
				{
					yield return edits[index];
				}
				else
				{
					yield return item;
				}
				index++;
			}
		}
	}
}