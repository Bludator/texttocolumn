using System.Collections.Generic;
using System.ComponentModel;

namespace TextToColumn
{
	/// <summary>
	///   Represents a column with enumerable string rows and a <see cref="State"/>
	/// </summary>
	public interface IColumn : IReadOnlyColumn
	{
		/// <summary>
		///   The state of this column
		/// </summary>
		new State State { get; set; }

		/// <summary>
		///   The cells of the column
		/// </summary>
		new IEnumerable<string> Cells { get; }

		/// <summary>
		///   Get edit of one cell of this column
		/// </summary>
		/// <param name="row"> desired row </param>
		/// <returns> </returns>
		string GetEdit(int row);

		/// <summary>
		///   Edit one cell of this column without modifying the source
		/// </summary>
		/// <param name="row"> row to edit </param>
		/// <param name="newText"> new text of the row </param>
		void SetEdit(int row, string newText);
	}

	/// <summary>
	///   Represents a column with enumerable string rows and a <see cref="State"/>
	/// </summary>
	public interface IReadOnlyColumn : IEnumerable<string>, INotifyPropertyChanged
	{
		/// <summary>
		///   The state of this column
		/// </summary>
		State State { get; }

		IEnumerable<string> Cells { get; }
	}
}