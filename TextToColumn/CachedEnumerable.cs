using System;
using System.Collections;
using System.Collections.Generic;

namespace TextToColumn
{
	/// <summary>
	///   Caches input `IEnumerable`
	/// </summary>
	/// <remarks>
	///   Also fixes bugs ("known issues") in `File.ReadLines()`, where multiple calls to the method returns the same
	///   enumerator and thus doesn`t read file from the beginning.
	/// </remarks>
	public class CachedEnumerable<T> : IEnumerable<T>, IDisposable
	{
		private readonly List<T> cache = new List<T>();
		private readonly IEnumerator<T> input;
		private bool _DisposedValue;

		/// <summary>
		/// </summary>
		/// <param name="input"> Enumerable to cache </param>
		public CachedEnumerable(IEnumerable<T> input)
		{
			this.input = input.GetEnumerator();
		}

		///<inheritdoc/>
		public IEnumerator<T> GetEnumerator()
		{
			int index = 0;
			while (true)
			{
				if (cache.Count > index)
				{
					yield return cache[index];
				}
				else
				{
					if (input.MoveNext())
					{
						cache.Add(input.Current);
						yield return input.Current;
					}
					else
					{
						input.Dispose();
						yield break;
					}
				}
				index++;
			}
		}

		IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();

		protected virtual void Dispose(bool disposing)
		{
			if (!_DisposedValue)
			{
				if (disposing)
				{
					input.Dispose();
					cache.Clear();
				}
				_DisposedValue = true;
			}
		}

		/// <summary>
		///   Release cache and internal enumerator
		/// </summary>
		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}
	}
}