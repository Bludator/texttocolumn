namespace TextToColumn
{
	/// <summary>
	///   Object with Undo/Redo capabilities
	/// </summary>
	/// <typeparam name="TCommand"> </typeparam>
	public interface IUndoable<TCommand>
	{
		/// <summary>
		///   Return true if it is possible to <see cref="Redo"/>
		/// </summary>
		public bool CanRedo { get; }

		/// <summary>
		///   Return true if it is possible to <see cref="Undo"/>
		/// </summary>
		public bool CanUndo { get; }

		/// <summary>
		///   Execute <paramref name="command"/>
		/// </summary>
		/// <param name="command"> </param>
		public void Do(TCommand command);

		/// <summary>
		///   Undo last command added with <see cref="Do(TCommand)"/> or <see cref="Redo"/>
		/// </summary>
		public void Undo();

		/// <summary>
		///   Redo last undone command added with <see cref="Undo"/>
		/// </summary>
		public void Redo();
	}
}