using System;
using System.Collections.Generic;
using System.Linq;

namespace TextToColumn
{
	public static class EnumerableExtension
	{
		public enum InversePolicy
		{
			/// <summary>
			///   Fill missing spaces (to rectangular matrix) with
			///   <code> default </code>
			/// </summary>
			FillWithDefault,

			/// <summary>
			///   Skip missing spaces (to rectangular matrix)
			/// </summary>
			Omit,
		}

		/// <summary>
		///   Return index of first element matching condition
		/// </summary>
		/// <typeparam name="TItem"> </typeparam>
		/// <param name="items"> collection of to search in </param>
		/// <param name="matchCondition"> condition to match each element against </param>
		/// <returns> index of first element matching condition or -1 if not found </returns>
		public static int IndexOfFirst<T>(this IEnumerable<T> items, Func<T, bool> matchCondition)
		{
			var index = 0;
			foreach (var item in items)
			{
				if (matchCondition.Invoke(item))
				{
					return index;
				}
				index++;
			}
			return -1;
		}

		/// <summary>
		///   Perform matrix-like inversion. But unlike matrices this table could be jagged, in which case inversion is
		///   carried out according to <see cref="InversePolicy"/>
		/// </summary>
		/// <typeparam name="T"> </typeparam>
		/// <param name="table"> table to inverse </param>
		/// <param name="policy"> policy what to do in case of missing cells (to rectangular table) </param>
		/// <returns> </returns>
		public static IEnumerable<T[]> InverseEnumerables<T>(this IEnumerable<IEnumerable<T>> table, InversePolicy policy = InversePolicy.FillWithDefault)
		{
			List<IEnumerator<T>> enumerators = new List<IEnumerator<T>>();
			try
			{
				foreach (var line in table)
				{
					enumerators.Add(line.GetEnumerator());
				}
				bool moved;
				while (true)
				{
					moved = false;
					T[] newLine;

					newLine = enumerators.Select<IEnumerator<T>, (T item, bool isFiller)>(e =>
					{
						if (e.MoveNext())
						{
							moved = true;
							return (e.Current, false);
						}
						else
						{
							return (default, true);
						}
					})
					.Where(x => !(policy == InversePolicy.Omit && x.isFiller))
					.Select(x => x.item)
					.ToArray();

					if (moved)
					{
						yield return newLine;
					}
					else
					{
						yield break;
					}
				}
			}
			finally
			{
				enumerators.ForEach(e => e.Dispose());
			}
		}

		public static IEnumerable<T> Cache<T>(this IEnumerable<T> input) => new CachedEnumerable<T>(input);
	}
}