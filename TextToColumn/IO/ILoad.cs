using System.Collections.Generic;

namespace TextToColumn.IO
{
	/// <summary>
	///   Represents source of columns
	/// </summary>
	public interface ILoad
	{
		/// <summary>
		///   Load data to list of enumerables
		/// </summary>
		/// <returns> Parsed enumerable for every columns </returns>
		IList<IEnumerable<string>> Load();
	}
}