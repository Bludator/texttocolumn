using System;
using System.Collections.Generic;
using System.Linq;
using TextToColumn.ViewModel;

namespace TextToColumn.IO
{
	/// <summary>
	///   Wraps methods for save/load simple text
	/// </summary>
	public class Text : ILoad, ISave
	{
		private readonly Action<IEnumerable<string>> target;
		private readonly IEnumerable<string> source;

		/// <summary>
		///   Initialize for saving
		/// </summary>
		/// <param name="target"> target to save </param>
		public Text(Action<IEnumerable<string>> target) => this.target = target;

		/// <summary>
		///   Initialize for loading
		/// </summary>
		/// <param name="source"> source of data, enumerating lines </param>
		public Text(IEnumerable<string> source) => this.source = source;

		/// <summary>
		///   Loads text into one <see cref="IColumn"/>
		/// </summary>
		/// <returns> A <see cref="IColumn"/> with rows from source </returns>
		public IList<IEnumerable<string>> Load() => new[] { source };

		/// <summary>
		///   Saves all column merged.
		/// </summary>
		/// <param name="table"> table to save </param>
		public void Save(TableVM table)
			 => target(table.Columns.Where(x => x.State == State.Ok).InverseEnumerables().Select(x => string.Concat(x)));
	}
}