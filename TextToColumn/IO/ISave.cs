using TextToColumn.ViewModel;

namespace TextToColumn.IO
{
	/// <summary>
	///   Represents save target
	/// </summary>
	public interface ISave
	{
		/// <summary>
		///   Save table
		/// </summary>
		/// <param name="table"> </param>
		/// <returns> </returns>
		void Save(TableVM table);
	}
}