using System;
using System.Collections.Generic;
using System.Linq;
using TextToColumn.ViewModel;

namespace TextToColumn.IO
{
	/// <summary>
	///   Wraps methods for parsing and emitting csv
	/// </summary>
	public class CSV : ISave, ILoad
	{
		private readonly Action<IEnumerable<string>> target;
		private readonly IEnumerable<string> source;

		/// <summary>
		///   Initialize for saving
		/// </summary>
		/// <param name="target"> target to save </param>
		public CSV(Action<IEnumerable<string>> target) => this.target = target;

		/// <summary>
		///   Initialize for loading
		/// </summary>
		/// <param name="source"> source of data, enumerating lines </param>
		public CSV(IEnumerable<string> source) => this.source = source;

		/// <summary>
		///   Loads table from csv file
		/// </summary>
		/// <returns> parsed columns </returns>
		public IList<IEnumerable<string>> Load()
		{
			throw new System.NotImplementedException(); //undone
		}

		/// <summary>
		///   Saves table in csv format
		/// </summary>
		/// <param name="table"> table to save </param>
		public void Save(TableVM table)
		{
			if (table.Columns.Count() == 0)
			{
				return;
			}
			target(CSVEnumerable(table));
		}

		private IEnumerable<string> CSVEnumerable(TableVM table)
		{
			char separator = ',';

			foreach (var row in table.Columns.Where(x => x.State == State.Ok).InverseEnumerables())
			{
				string line = "";
				foreach (var cell in row)
				{
					line += Sanitize(cell) + separator;
				}
				yield return line.Remove(line.Length - 1);
			}
		}

		private string Sanitize(string input)
		{
			if (input == null) return "";

			//https://tools.ietf.org/html/rfc4180
			if (input.Contains('"') || input.Contains('\n') || input.Contains('\r') || input.Contains(','))
			{
				return "\"" + input.Replace("\"", "\"\"") + "\""; //escape
			}
			return input;
		}
	}
}