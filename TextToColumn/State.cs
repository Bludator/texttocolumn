using System;

namespace TextToColumn
{
	/// <summary>
	///   Describes a state of a column
	/// </summary>
	[Flags]
	public enum State
	{
		/// <summary>
		///   It is ok to display
		/// </summary>
		Ok = 0,

		/// <summary>
		///   Column was removed by a transformation
		/// </summary>
		ImplicitlyRemoved = 1,

		/// <summary>
		///   Column was removed by the user
		/// </summary>
		ExplicitlyRemoved = 2,

		/// <summary>
		///   Column was used by a transformation
		/// </summary>
		Used = 4,

		/// <summary>
		///   Column is placeholder to newly transform-generated columns
		/// </summary>
		Placeholder = 8,
	}
}